<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';

if (isset($_POST['submit_save'])) {
    echo '<meta charset="utf-8">';
    $quo_id = NextQuo_id();
    $sql = 'INSERT INTO quotation '
            . '(quo_id, quo_date, cus_id, quo_product, quo_count, quo_unit, startpoint, endpoint, quo_distance, quo_startdate, quo_enddate, quo_number, trucktype_id, truck_id, drv_id, quo_price, quo_total, quo_status, quo_fuel_value, quo_fuel_price, quo_fuel_total, quo_rent, quo_drv_price, quo_drv_per, quo_name, quo_address, quo_tel, quo_mail, quo_line) '
            . 'VALUES '
            . '("' . $quo_id . '", NOW(),"' . $_SESSION['cus_id'] . '","' . $_SESSION['quo']['quo_product'] . '","' . $_SESSION['quo']['quo_count'] . '","' . $_SESSION['quo']['quo_unit'] . '","' . $_SESSION['quo']['startpoint'] . '","' . $_SESSION['quo']['endpoint'] . '","' . $_SESSION['quo']['quo_distance'] . '","' . DateFormatDB($_SESSION['quo']['quo_startdate']) . '","' . DateFormatDB($_SESSION['quo']['quo_enddate']) . '","' . $_SESSION['quo']['quo_number'] . '","' . $_SESSION['quo']['trucktype_id'] . '","' . $_SESSION['quo']['truck_id'] . '","' . $_POST['drv_id'] . '","' . $_POST['quo_price'] . '","' . $_POST['quo_total'] . '","0", "' . $_POST['quo_fuel_value'] . '", "' . $_POST['quo_fuel_price'] . '","' . $_POST['quo_fuel_total'] . '","' . $_POST['quo_rent'] . '", "' . $_POST['quo_drv_price'] . '", "' . $_POST['quo_drv_per'] . '", "' . $_SESSION['quo']['quo_name'] . '", "' . $_SESSION['quo']['quo_address'] . '", "' . $_SESSION['quo']['quo_tel'] . '", "' . $_SESSION['quo']['quo_mail'] . '", "' . $_SESSION['quo']['quo_line'] . '") ';

    $result = mysql_query($sql);
    if ($result) {
        unset($_SESSION['cus_id']);
        unset($_SESSION['quo']);

        echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!");window.open("quotation_print.php?id=' . $quo_id . '","_blank");window.focus();</script>';
        echo '<meta http-equiv="refresh" content="1; URL = quotation.php"/>';
        exit();
    } else {
        echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ !!!");window.history.back();</script>';
        exit();
    }
}

if (!isset($_POST['quo_count'])) {
    echo '<meta http-equiv="refresh" content="1; URL = quotation.php"/>';
    exit();
}

$quoArrayObject = new ArrayObject($_POST);
$_SESSION['quo'] = $quoArrayObject->getArrayCopy();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="lib/pagination/style.css" rel="stylesheet" type="text/css"/>        
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="quotation.php">ใบเสนอราคา</a></li>
                    <li class="active">จัดทำใบเสนอราคา (2. กำหนดรายละเอียด)</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ใบเสนอราคา</h1>
                </div>
            </div><!--/.row-->

            <form name="quotationform" id="quotationform" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">                        
                            <div class="panel-body"> 
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <?php echo COMPANY_NAME; ?>

                                        <h2>ตัวอย่างใบเสนอราคา</h2>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <?php
                                        $sql = 'SELECT * FROM customer WHERE cus_id = "' . $_SESSION['cus_id'] . '"';
                                        $result = mysql_query($sql);
                                        $row = mysql_fetch_array($result);
                                        ?>
                                        <p><strong>รหัสลูกค้า : </strong><?php echo $row['cus_id']; ?></p>
                                        <p><strong>ชื่อบริษัท : </strong><?php echo $row['cus_name']; ?></p>
                                        <p><strong>ที่อยู่ : </strong><?php echo $row['cus_address']; ?></p>
                                        <p><strong>เบอร์โทร : </strong><?php echo $row['cus_tel']; ?></p>
                                        <p><strong>E-mail : </strong><?php echo $row['cus_email']; ?> <strong>Line-id : </strong><?php echo $row['cus_line']; ?></p>
                                        <p><strong>ผู้ประสานงาน : </strong><?php echo $row['cus_contact']; ?></p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <p><strong>เลขที่ : </strong><?php echo NextQuo_id(); ?></p>
                                        <p><strong>วันที่ : </strong><?php echo ThaidateNoTime(date('Y-m-d')); ?></p>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">รายละเอียด</th>
                                                <th width="100" class="text-center">จำนวนเที่ยว</th>
                                                <th width="200" class="text-center">ราคาต่อเที่ยว</th>
                                                <th width="200" class="text-center">ราคารวม</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <?php
                                                    echo '<p><strong>ข้อมูลปลายทาง</strong></p>';
                                                    echo 'ชื่อ-นามสกุล : ' . $_SESSION['quo']['quo_name'] . ' <br>';
                                                    echo 'ที่อยู่ : ' . $_SESSION['quo']['quo_address'] . ' <br>';
                                                    echo 'เบอร์โทร : ' . $_SESSION['quo']['quo_tel'] . ' <br>';
                                                    echo 'E-mail : ' . $_SESSION['quo']['quo_mail'] . ' Line ID : ' . $_SESSION['quo']['quo_line'] . '<br><br>';
                                                    
                                                    echo '<p><strong>รายละเอียดสินค้า</strong></p>';
                                                    echo 'ขนส่ง ' . $_SESSION['quo']['quo_product'] . ' ';
                                                    echo 'จำนวน ' . $_SESSION['quo']['quo_count'] . ' ' . $_SESSION['quo']['quo_unit'] . ' ';
                                                    echo '<br>';
                                                    echo 'ต้นทาง ' . Province_name($_SESSION['quo']['startpoint']) . ' ถึงปลายทาง ' . Province_name($_SESSION['quo']['endpoint']) . ' ';
                                                    echo '<br>';
                                                    echo 'รวมระยะทาง ' . $_SESSION['quo']['quo_distance'] . ' กิโลเมตร';
                                                    echo '<br>';
                                                    echo 'ตั้งแต่วันที่ ' . ThaidateNoTime2($_SESSION['quo']['quo_startdate']) . ' ถึงวันที่ ' . ThaidateNoTime2($_SESSION['quo']['quo_enddate']) . ' ';
                                                    echo '<br>';
                                                    echo '<br>';
                                                    echo 'ใช้ ' . GetTrucktype($_SESSION['quo']['trucktype_id']) . ' ทะเบียนรถ ' . GetTruck($_SESSION['quo']['truck_id']);
                                                    echo '<br>';
                                                    echo 'พนักงานขับรถ ' . $_SESSION['quo']['drv_name'];
                                                    ?>
                                                </td>
                                                <td class="text-center"  style="vertical-align: top;"><?php echo $_SESSION['quo']['quo_number']; ?></td>
                                                <td class="text-center"  style="vertical-align: top;">
                                                    <?php
                                                    $quo_rs = Quo_price($_SESSION['quo']['quo_distance'], $_SESSION['quo']['quo_fuel'], $_SESSION['quo']['truck_id']);
                                                    $quo_price = $quo_rs['quo_price'];
                                                    echo number_format($quo_price, 2);
                                                    ?>
                                                </td>
                                                <td class="text-right" style="vertical-align: top;">
                                                    <?php
                                                    $quo_total = $quo_price * $_SESSION['quo']['quo_number'];
                                                    echo number_format($quo_total, 2);
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" class="text-right">
                                                    <strong>รวมทั้งสิ้น</strong>
                                                    <input type="hidden" name="quo_price" value="<?php echo $quo_price ?>">
                                                    <input type="hidden" name="quo_fuel_value" value="<?php echo $quo_rs['quo_fuel_value'] ?>">
                                                    <input type="hidden" name="quo_fuel_price" value="<?php echo $quo_rs['quo_fuel_price'] ?>">
                                                    <input type="hidden" name="quo_fuel_total" value="<?php echo $quo_rs['quo_fuel_total'] ?>">
                                                    <input type="hidden" name="quo_rent" value="<?php echo $quo_rs['quo_rent'] ?>">
                                                    <input type="hidden" name="quo_drv_price" value="<?php echo $quo_rs['quo_drv_price'] ?>">
                                                    <input type="hidden" name="quo_drv_per" value="<?php echo $quo_rs['quo_drv_per'] ?>">
                                                    <input type="hidden" name="quo_total" value="<?php echo $quo_total ?>">
                                                    <input type="hidden" name="drv_id" value="<?php echo GetDriver_idByname($_SESSION['quo']['drv_name']); ?>">
                                                </td>
                                                <td class="text-right"><strong><?php echo number_format($quo_total, 2); ?></strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <hr>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary" name="submit_save" id="submit"><span class="glyphicon glyphicon-save"></span> บันทึก</button>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a class="btn btn-warning" href="quotation_add2.php?id=<?php echo $_SESSION['cus_id']; ?>" title="แก้ไข"><span class="glyphicon glyphicon-pencil"></span> แก้ไข</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a class="btn btn-danger" href="quotation_cancel.php" title="ยกเลิก" onclick="return confirm('ยกเลิกรายการนี้หรือไม่ ?');"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col-->
                    </div>
                </div>
            </form>
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-table.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript">
                                            !function ($) {
                                                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                                                    $(this).find('em:first').toggleClass("glyphicon-minus");
                                                });
                                                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
                                            }(window.jQuery);

                                            $(window).on('resize', function () {
                                                if ($(window).width() > 768)
                                                    $('#sidebar-collapse').collapse('show');
                                            });
                                            $(window).on('resize', function () {
                                                if ($(window).width() <= 767)
                                                    $('#sidebar-collapse').collapse('hide');
                                            });

                                            $(function () {
                                                Truck_id();
                                                $('#trucktype_id').change(function () {
                                                    Truck_id();
                                                    $('#drv_name').val("");
                                                    $('#drv_id').val("");
                                                });
                                                $('#truck_id').change(function () {
                                                    Driver_name();
                                                });

                                                Endpoint();
                                                $('#startpoint').change(function () {
                                                    Endpoint();
                                                    Distance();
                                                });

                                                $('#endpoint').change(function () {
                                                    Distance();
                                                });

                                                $('#quotationform').validate({
                                                    rules: {
                                                        quo_product: {
                                                            required: true
                                                        },
                                                        quo_count: {
                                                            required: true,
                                                            number: true,
                                                            min: 1
                                                        },
                                                        quo_unit: {
                                                            required: true
                                                        },
                                                        startpoint: {
                                                            required: true
                                                        },
                                                        endpoint: {
                                                            required: true
                                                        },
                                                        quo_distance: {
                                                            required: true,
                                                            number: true,
                                                            min: 1
                                                        },
                                                        quo_startdate: {
                                                            required: true
                                                        },
                                                        quo_enddate: {
                                                            required: true
                                                        },
                                                        quo_number: {
                                                            required: true,
                                                            number: true,
                                                            min: 1
                                                        },
                                                        trucktype_id: {
                                                            required: true
                                                        },
                                                        truck_id: {
                                                            required: true
                                                        },
                                                        drv_name: {
                                                            required: true
                                                        }
                                                    },
                                                    messages: {
                                                        quo_product: {
                                                            required: 'กรอกรายละเอียดสินค้า'
                                                        },
                                                        quo_count: {
                                                            required: 'กรอกจำนวน',
                                                            number: 'เป็นตัวเลขเท่านั้น',
                                                            min: 'อย่างน้อย 1'
                                                        },
                                                        quo_unit: {
                                                            required: 'เลือกหน่วย'
                                                        },
                                                        startpoint: {
                                                            required: 'เลือกจังหวัดต้นทาง'
                                                        },
                                                        endpoint: {
                                                            required: 'เลือกจังหวัดปลายทาง'
                                                        },
                                                        quo_distance: {
                                                            required: 'เลือกต้นทาง-ปลายทาง',
                                                            number: 'เป็นตัวเลขเท่านั้น',
                                                            min: 'อย่างน้อย 1'
                                                        },
                                                        quo_startdate: {
                                                            required: 'เลือกวันที่เริ่มต้น'
                                                        },
                                                        quo_enddate: {
                                                            required: 'เลือกวันที่สิ้นสุด'
                                                        },
                                                        quo_number: {
                                                            required: 'กรอกจำนวนเที่ยว',
                                                            number: 'เป็นคัวเลขเท่านั้น',
                                                            min: 'อย่างน้อย 1'
                                                        },
                                                        trucktype_id: {
                                                            required: 'เลือกประเภทรถบรรทุก'
                                                        },
                                                        truck_id: {
                                                            required: 'เลือกรถบรรทุก'
                                                        },
                                                        drv_name: {
                                                            required: 'เลือกประเภทรถบรรทุก และรถบรรทุก'
                                                        }
                                                    }
                                                });
                                            });


                                            function Truck_id() {
                                                $.post('lib/truck_id_quo.php', {'trucktype_id': $('#trucktype_id').val()},
                                                function (data) {
                                                    $('#truck_id').html(data);
                                                });
                                            }

                                            function Driver_name() {
                                                $.post('lib/driver_name.php', {'truck_id': $('#truck_id').val()},
                                                function (data) {
                                                    $('#drv_name').val(data);
                                                });
                                            }

                                            function Endpoint() {
                                                $.post('lib/endpoint.php', {startpoint: $('#startpoint').val()},
                                                function (data) {
                                                    $('#endpoint').html(data);
                                                });
                                            }

                                            function Distance() {
                                                $.post('lib/distance.php', {startpoint: $('#startpoint').val(), endpoint: $('#endpoint').val()},
                                                function (data) {
                                                    $('#quo_distance').val(data);
                                                });
                                            }
        </script>
    </body>
</html>
