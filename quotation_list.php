<?php
include 'lib/config.php';
include 'lib/conn.php';
include 'lib/function.php';
?>
<script type="text/javascript">
    $(function () {
        $('.usr').click(function () {
            $.post('quotation_detail.php', {id: $(this).data('id')},
            function (data) {
                $('#cusdata').html(data);
            });
        });
    });
</script>

<?php
if ($_POST['page']) {
    $page = $_POST['page'];
    $cur_page = $page;
    $page -= 1;
    $per_page = 20;
    $previous_btn = TRUE;
    $next_btn = TRUE;
    $first_btn = TRUE;
    $last_btn = TRUE;
    $start = $page * $per_page;

    $opt = '';

//    if ($_POST['search-text'] != '') {
//        $opt .= ' AND (cus_name LIKE "%' . $_POST['search-text'] . '%" OR '
//                . 'cus_id LIKE "%' . $_POST['search-text'] . '%" OR '
//                . 'cus_contact LIKE "%' . $_POST['search-text'] . '%" OR '
//                . 'cus_about LIKE "%' . $_POST['search-text'] . '%")';
//        echo '<p align="center"><strong>ผลการค้นหา "' . $_POST['search-text'] . '"<br>';
//        echo '<a href="">แสดงทั้งหมด</a></strong></p>';
//    }
    ?>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="100" class="text-center">ลำดับที่</th>
                    <th class="text-center">เลขที่ใบเสนอราคา</th>
                    <th class="text-center">วันที่</th>
                    <th class="text-center">บริษัท</th>
                    <th width="100" class="text-center">จำนวนเงิน</th>
                    <th width="100" class="text-center">สถานะ</th>
                    <th width="100" class="text-center">รายละเอียด</th>
                    <th width="100" class="text-center">พิมพ์</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = 'SELECT * FROM customer, quotation '
                        . 'WHERE '
                        . 'customer.cus_id = quotation.cus_id ' . $opt
                        . 'ORDER BY quo_id DESC '
                        . 'LIMIT ' . $start . ',' . $per_page;
                $result = mysql_query($sql);
                if (mysql_num_rows($result) == 0) {
                    echo '<tr><td colspan="8" class="text-danger" align="center">ไม่พบข้อมูล</td></tr>';
                } else {
                    $i = 1;
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $i + $start; ?></td>
                            <td class="text-center"><?php echo $row['quo_id']; ?></td>
                            <td class="text-center"><?php echo ThaidatenoTime($row['quo_date']); ?></td>
                            <td><?php echo $row['cus_name']; ?></td>
                            <td class="text-center"><?php echo number_format($row['quo_total'], 2); ?></td>
                            <td class="text-center"><?php echo $quo_status[$row['quo_status']]; ?></td>
                            <td class="text-center">
                                <a class="btn btn-info btn-sm usr" title="รายละเอียด" data-toggle="modal" data-target="#myModal" data-id="<?php echo $row['quo_id']; ?>">
                                    <span class="glyphicon glyphicon-th-large"></span>
                                </a> 
                            </td>
                            <td class="text-center">
                                <a class="btn btn-warning btn-sm" href="quotation_print.php?id=<?php echo $row['quo_id']; ?>" title="พิมพ์" target="_blank"> <span class="glyphicon glyphicon-print"></span></a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
            </tbody>
        </table>
    </div>

    <?php
    $query_pag_num = 'SELECT COUNT(*) AS count FROM customer, quotation '
            . 'WHERE '
            . 'customer.cus_id = quotation.cus_id ' . $opt;
    $result_pag_num = mysql_query($query_pag_num);
    $row = mysql_fetch_array($result_pag_num);
    $count = $row['count'];
    $no_of_paginations = ceil($count / $per_page);

    include 'lib/pagination/pagination.php';
}