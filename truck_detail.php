<?php
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/function.php';
?>
<div class="row">
    <?php
    $sql = 'SELECT * FROM truck, trucktype, fuel, province '
            . 'WHERE '
            . 'truck.fuel_id = fuel.fuel_id AND '
            . 'trucktype.trucktype_id = truck.trucktype_id AND '
            . 'province.province_id = truck.province_id AND '
            . 'truck.truck_id = "' . $_POST['id'] . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    ?>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>รหัสรถบรรทุก :</strong></p></div>
        <div class="col-xs-6"><p><?php echo $row['truck_id']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>ทะเบียนรถ :</strong></p></div>
        <div class="col-xs-6"><p><?php echo $row['truck_license'] . '<br>' . $row['province_name']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>ประเภทรถบรรทุก :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['trucktype_name']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>พนักงานขับรถ :</strong><p></div>
        <div class="col-xs-6">
            <p>
                <?php
                $drivername = Getdriver($row['truck_id']);
                echo ($drivername == '') ? '<span class="text-danger">==ยังไม่มีพนักงานขับรถ==</span>' : $drivername;
                ?>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>เชื้อเพลิง :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['fuel_name']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>สิ้นเปลือง :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['truck_fueluse']; ?>  กม./ลิตร</p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>น้ำหนักบรรทุก :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['truck_load']; ?> ตัน</p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>สถานะ :</strong></p></div>
        <div class="col-xs-6"><p><?php echo $truck_status[$row['truck_status']]; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>วัน/เวลา ที่เพิ่ม :</strong><p></div>
        <div class="col-xs-6"><p><?php echo DatetimeThai($row['truck_registime']); ?></p></div>
    </div>

</div>
