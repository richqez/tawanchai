<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';

if (isset($_POST['submit_save'])) {
    echo '<meta charset="utf-8">';
    $invoice_id = NextInvoice_id();
    $sql = 'INSERT INTO invoice '
            . '(invoice_id, invoice_date, cus_id, invoice_month, invoice_year, invoice_total, invoice_status, invoice_due) '
            . 'VALUES '
            . '("' . $invoice_id . '", NOW(),"' . $_GET['id'] . '","' . $_GET['m'] . '","' . $_GET['y'] . '","' . $_POST['invoice_total'] . '", "0", "' . DateFormatDB($_POST['invoice_due']) . '") ';

    $result = mysql_query($sql);
    if ($result) {
        foreach ($_POST['quo_id'] as $v) {
            $sql = 'UPDATE quotation SET quo_invoice = "1" WHERE quo_id = "' . $v . '"';
            mysql_query($sql);
            $sql = 'INSERT INTO invoicedetail (invoice_id, quo_id) VALUES ("' . $invoice_id . '", "' . $v . '")';
            mysql_query($sql);
        }
        echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!");window.open("invoice_print.php?id=' . $invoice_id . '","_blank");window.focus();</script>';
        echo '<meta http-equiv="refresh" content="1; URL = invoice.php"/>';
        exit();
    } else {
        echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ !!!");window.history.back();</script>';
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="js/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="js/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="lib/pagination/style.css" rel="stylesheet" type="text/css"/>        
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <style>
            .table > tbody > tr > td {
                vertical-align: top;
            }
        </style>
    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="invoice.php">ใบแจ้งหนี้</a></li>
                    <li class="active">จัดทำใบแจ้งหนี้ (2. รายละเอียดค่าขนส่งสินค้า)</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ใบแจ้งหนี้</h1>
                </div>
            </div><!--/.row-->

            <form name="invoiceform" id="invoiceform" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">                        
                            <div class="panel-body"> 
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <?php echo COMPANY_NAME; ?>
                                        <h1 class="page-header">ใบแจ้งหนี้</h1>
                                        <h2>ค่าขนส่งประจำเดือน <?php echo $month_short[$_GET['m']] . ' ' . ($_GET['y'] + 543); ?></h2>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <?php
                                        $sql = 'SELECT * FROM customer WHERE cus_id = "' . $_GET['id'] . '"';
                                        $result = mysql_query($sql);
                                        $row = mysql_fetch_array($result);
                                        ?>
                                        <p><strong>รหัสลูกค้า : </strong><?php echo $row['cus_id']; ?></p>
                                        <p><strong>ชื่อบริษัท : </strong><?php echo $row['cus_name']; ?></p>
                                        <p><strong>ที่อยู่ : </strong><?php echo $row['cus_address']; ?></p>
                                        <p><strong>เบอร์โทร : </strong><?php echo $row['cus_tel']; ?></p>
                                        <p><strong>E-mail : </strong><?php echo $row['cus_email']; ?> <strong>Line ID : </strong><?php echo $row['cus_line']; ?></p>
                                        <p><strong>ผู้ประสานงาน : </strong><?php echo $row['cus_contact']; ?></p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <p><strong>เลขที่ : </strong><?php echo NextInvoice_id(); ?></p>
                                        <p><strong>วันที่ : </strong><?php echo ThaidateNoTime(date('Y-m-d')); ?></p>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="80" class="text-center">ลำดับที่</th>
                                                <th width="100" class="text-center">วันที่</th>
                                                <th class="text-center">รายละเอียด</th>
                                                <th width="100" class="text-center">จำนวนเที่ยว</th>
                                                <th width="150" class="text-center">ราคาต่อเที่ยว</th>
                                                <th width="200" class="text-center">ราคารวม</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql = 'SELECT * FROM customer, quotation '
                                                    . 'WHERE '
                                                    . 'quotation.cus_id = customer.cus_id AND '
                                                    . 'quotation.quo_status = "1" AND '
                                                    . 'quotation.quo_invoice = "0" AND '
                                                    . 'MONTH(quotation.quo_date) = ' . $_GET['m'] . ' AND '
                                                    . 'YEAR(quotation.quo_date) = ' . $_GET['y'] . ' AND '
                                                    . 'customer.cus_id = "' . $_GET['id'] . '" '
                                                    . 'ORDER BY quotation.quo_date ASC ';
                                            $result = mysql_query($sql);
                                            $i = 1;
                                            $quo_total = 0;
                                            while ($row = mysql_fetch_array($result)) {
                                                $quo_total += $row['quo_total'];
                                                ?>
                                                <tr>
                                                    <td class="text-center" style="vertical-align: top;"><?php echo $i; ?></td>
                                                    <td class="text-center" style="vertical-align: top;"><?php echo ThaidateNoTime($row['quo_date']); ?></td>
                                                    <td>
                                                        <?php
                                                        echo '<p><strong>ข้อมูลปลายทาง</strong></p>';
                                                        echo 'ชื่อ-นามสกุล : ' . $row['quo_name'] . ' <br>';
                                                        echo 'ที่อยู่ : ' . $row['quo_address'] . ' <br>';
                                                        echo 'เบอร์โทร : ' . $row['quo_tel'] . ' <br>';
                                                        echo 'E-mail : ' . $row['quo_mail'] . ' Line ID : ' . $row['quo_line'] . '<br><br>';

                                                        echo '<p><strong>รายละเอียดสินค้า</strong></p>';
                                                        echo 'ขนส่ง ' . $row['quo_product'] . ' ';
                                                        echo 'จำนวน ' . $row['quo_count'] . ' ' . $row['quo_unit'] . ' ';
                                                        echo '<br>';
                                                        echo 'ต้นทาง ' . Province_name($row['startpoint']) . ' ถึงปลายทาง ' . Province_name($row['endpoint']) . ' ';
                                                        echo '<br>';
                                                        echo 'รวมระยะทาง ' . $row['quo_distance'] . ' กิโลเมตร';
                                                        echo '<br>';
                                                        echo 'ตั้งแต่วันที่ ' . ThaidateNoTime($row['quo_startdate']) . ' ถึงวันที่ ' . ThaidateNoTime($row['quo_enddate']) . ' ';
                                                        echo '<br>';
                                                        echo '<br>';
                                                        echo 'ใช้ ' . GetTrucktype($row['trucktype_id']) . ' ทะเบียนรถ ' . GetTruck($row['truck_id']);
                                                        echo '<br>';
                                                        echo 'พนักงานขับรถ ' . Getdriver2($row['drv_id']);
                                                        ?>
                                                    </td>
                                                    <td class="text-center" style="vertical-align: top;"><?php echo $row['quo_number']; ?></td>
                                                    <td class="text-center" style="vertical-align: top;">
                                                        <input type="hidden" name="quo_id[]" value="<?php echo $row['quo_id']; ?>">
                                                        <?php
                                                        echo number_format($row['quo_price'], 2);
                                                        ?>
                                                    </td>
                                                    <td class="text-right" style="vertical-align: top;">
                                                        <?php
                                                        echo number_format($row['quo_total'], 2);
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5" class="text-right">
                                                    <strong>รวมทั้งสิ้น</strong>
                                                    <input type="hidden" name="invoice_total" value="<?php echo $quo_total ?>">
                                                </td>
                                                <td class="text-right"><strong><?php echo number_format($quo_total, 2); ?></strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-4">
                                        <div class="form-group">
                                            <label>กำหนดชำระเงิน</label>
                                            <input class="form-control" name="invoice_due" type="text" id="invoice_due" value="<?php echo date("d/m/Y", strtotime("+7 days"));   ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <hr>
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary" name="submit_save" id="submit"><span class="glyphicon glyphicon-save"></span> ออกใบแจ้งหนี้</button>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a class="btn btn-danger" href="invoice_add.php" title="ยกเลิก" onclick="return confirm('ยกเลิกรายการนี้หรือไม่ ?');"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col-->
                    </div>
                </div>
            </form>
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-table.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-ui.js" type="text/javascript"></script>
        <script src="js/jquery.ui.datepicker.validation.min.js" type="text/javascript"></script>
        <script src="js/datepicker/js/moment-with-locales.js" type="text/javascript"></script>        
        <script src="js/datepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script type="text/javascript">
                                            !function ($) {
                                                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                                                    $(this).find('em:first').toggleClass("glyphicon-minus");
                                                });
                                                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
                                            }(window.jQuery);

                                            $(window).on('resize', function () {
                                                if ($(window).width() > 768)
                                                    $('#sidebar-collapse').collapse('show');
                                            });
                                            $(window).on('resize', function () {
                                                if ($(window).width() <= 767)
                                                    $('#sidebar-collapse').collapse('hide');
                                            });

                                            $(function () {


                                                $('#invoiceform').validate({
                                                    rules: {
                                                        invoice_due: {
                                                            required: true
                                                        }
                                                    },
                                                    messages: {
                                                        invoice_due: {
                                                            required: 'กรอกรายละเอียดสินค้า'
                                                        }
                                                    }
                                                });
                                            });
        </script>
        <script>
            $(function () {
                $('#invoice_due').datetimepicker({
                    format: 'D/M/YYYY',
                    locale: 'th',
                    minDate: moment()
                });
            });
        </script>
    </body>
</html>
