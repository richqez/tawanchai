<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/class.upload.php';

if (isset($_POST['submit'])) {

    $drv_id = NextDrv_id();
    echo '<meta charset="utf-8">';
    if ($_FILES['drv_image']['name'] == '') {
        $image_name = 'human.jpg';
    } else {
        $upload_image = new upload($_FILES['drv_image']);
        if ($upload_image->uploaded) {
            $upload_image->image_resize = true;
            $upload_image->image_x = 100;
            $upload_image->image_ratio_y = true;
            $upload_image->file_new_name_body = $drv_id;
            $upload_image->process("images/driver");

            if ($upload_image->processed) {
                $image_name = $upload_image->file_dst_name;
                $upload_image->clean();
            } else {
                echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกรูปภาพได้ !!!");window.history.back();</script>';
                exit();
            }
        }
    }
    $sql = 'INSERT INTO driver '
            . '(drv_id, drv_name, drv_idcard, drv_licensecar, drv_birthday, drv_address, '
            . 'drv_tel, drv_email, drv_registime, drv_status, truck_id, drv_image)'
            . 'VALUES '
            . '("' . $drv_id . '", '
            . '"' . trim($_POST['drv_name']) . '", '
            . '"' . trim($_POST['drv_idcard']) . '", '
            . '"' . trim($_POST['drv_licensecar']) . '", '
            . '"' . trim($_POST['drv_birthday']) . '", '
            . '"' . trim($_POST['drv_address']) . '", '
            . '"' . trim($_POST['drv_tel']) . '", '
            . '"' . trim($_POST['drv_email']) . '", '
            . 'NOW(), '
            . '"1", "' . $_POST['truck_id'] . '",'
            . '"' . $image_name . '") ';
    $result = mysql_query($sql);
    if ($result) {
        $sql = 'UPDATE truck SET truck_drv = "1" WHERE truck_id = "' . $_POST['truck_id'] . '"';
        mysql_query($sql);
        echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!")</script>';
        echo '<meta http-equiv="refresh" content="1; URL = driver.php"/>';
        exit();
    } else {
        echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ !!!");window.history.back();</script>';
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="driver.php">พนักงานขับรถ</a></li>
                    <li class="active">เพิ่มพนักงานขับรถ</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">พนักงานขับรถ</h2>
                </div>
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-plus"></span> เพิ่มพนักงานขับรถ</div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3">
                                <form name="driverform" id="driverform" action="" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>ชื่อ-นามสกุล</label>
                                        <input class="form-control" name="drv_name" type="text" id="drv_name" placeholder="ชื่อ-นามสกุล *"> 
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>เลขประจำตัวประชาชน</label>
                                            <input class="form-control" name="drv_idcard" type="text" id="drv_idcard" placeholder="เลขประจำตัวประชาชน *" maxlength="13"> 
                                        </div>
                                        <div class="col-md-6">
                                            <label>เลขที่ใบขับขี่</label>
                                            <input class="form-control" name="drv_licensecar" type="text" id="drv_licensecar" placeholder="เลขที่ใบขับขี่ *" maxlength="8"> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>วัน-เดือน-ปี เกิด</label>
                                        <input class="form-control" name="drv_birthday" type="date" id="drv_birthday" placeholder="วัน-เดือน-ปี เกิด *"> 
                                    </div>
                                    <div class="form-group">
                                        <label>ที่อยู่</label>
                                        <textarea class="form-control" name="drv_address" id="drv_address" placeholder="ที่อยู่ *" rows="3"></textarea>    
                                    </div>
                                    <div class="form-group">
                                        <label>เบอร์โทรศัพท์</label>
                                        <input class="form-control" name="drv_tel" type="text" id="drv_tel" placeholder="เบอร์โทรศัพท์ *" maxlength="10"> 
                                    </div>

                                    <div class="form-group">
                                        <label>อีเมล์</label>
                                        <input class="form-control" name="drv_email" type="email" id="drv_email" placeholder="อีเมล์ *"> 
                                    </div>

                                    <div class="form-group">
                                        <label>รูปภาพ</label>
                                        <input type="file" class="form-control" id="drv_image" name="drv_image" placeholder="รูปภาพ">
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>ประเภทรถบรรทุก</label>
                                            <select class="form-control" id="trucktype_id" name="trucktype_id">
                                                <?php Trucktype_id(); ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>ทะเบียนรถบรรทุก</label>
                                            <select class="form-control" id="truck_id" name="truck_id"></select>
                                        </div>
                                    </div>
                                    <p>&nbsp;</p>

                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> บันทึก</button>		
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div><!-- /.row -->
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('#driverform').validate({
                    rules: {
                        drv_name: {
                            required: true
                        },
                        drv_idcard: {
                            required: true,
                            number: true,
                            minlength: 13
                        },
                        drv_licensecar: {
                            required: true,
                            number: true,
                            minlength: 8
                        },
                        drv_birthday: {
                            required: true
                        },
                        drv_address: {
                            required: true
                        },
                        drv_tel: {
                            required: true,
                            number: true,
                            minlength: 10
                        },
                        drv_email: {
                            email: true
                        },
                        drv_image: {
                            accept: "jpeg|png|jpg|bmp"
                        },
                        trucktype_id: {
                            required: true
                        },
                        truck_id: {
                            required: true
                        }
                    },
                    messages: {
                        drv_name: {
                            required: 'กรอกชื่อ-นามสกุล'
                        },
                        drv_idcard: {
                            required: 'กรอกเลขประจำตัวประชาชน',
                            number: 'เป็นตัวเลขเท่านั้น',
                            minlength: 'ต้องเป็น 13 หลัก'
                        },
                        drv_licensecar: {
                            required: 'กรอกเบอร์โทรศัพท์',
                            number: 'เป็นตัวเลขเท่านั้น',
                            minlength: 'ต้องเป็น 8 หลัก'
                        },
                        drv_birthday: {
                            required: 'เลือกวัน-เดือน-ปี เกิด'
                        },
                        drv_address: {
                            required: 'กรอกที่อยู่'
                        },
                        drv_tel: {
                            required: 'กรอกเบอร์โทรศัพท์',
                            number: 'เป็นตัวเลขเท่านั้น',
                            minlength: 'ต้องเป็น 10 หลัก'
                        },
                        drv_email: {
                            required: 'กรอกอีเมล์',
                            email: 'รูปแบบอีเมล์ไม่ถูกต้อง'
                        },
                        trucktype_id: {
                            required: 'เลือกประเภทรถบรรทุก'
                        },
                        truck_id: {
                            required: 'เลือกรถบรรทุก'
                        },
                        drv_image: {
                            accept: 'ต้องเป็นไฟล์รูปภาพเท่านั้น (.jpg,.png,.jpeg,.bmp)'
                        }
                    }
                });

                function Truck_id() {
                    $.post('lib/truck_id.php', {'trucktype_id': $('#trucktype_id').val(), 'truck_id': ''},
                    function (data) {
                        $('#truck_id').html(data);
                    });
                }

                Truck_id();
                $('#trucktype_id').change(function () {
                    Truck_id();
                });
            });
        </script>
    </body>
</html>
