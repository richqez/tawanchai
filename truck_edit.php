<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/class.upload.php';

if (isset($_POST['submit'])) {
    echo '<meta charset="utf-8">';
    $sql = 'SELECT * FROM truck WHERE truck_license = "' . trim($_POST['truck_license']) . '" AND province_id = "' . $_POST['province_id'] . '" AND truck_id != "' . $_GET['id'] . '"';
    $recheack = mysql_query($sql);
    if (mysql_num_rows($recheack) > 0) {
        echo '<script>alert("มีทะเบียนรถนี้ในระบบแล้ว โปรดตรวจสอบ !!!");window.history.back();</script>';
        exit();
    }

    $sql = 'UPDATE truck SET '
            . 'truck_license = "' . trim($_POST['truck_license']) . '", '
            . 'province_id = "' . $_POST['province_id'] . '", '
            . 'trucktype_id = "' . $_POST['trucktype_id'] . '", '
            . 'fuel_id = "' . $_POST['fuel_id'] . '", '
            . 'truck_fueluse = "' . trim($_POST['truck_fueluse']) . '", '
            . 'truck_load = "' . trim($_POST['truck_load']) . '", '
            . 'truck_rent = ' . $_POST['truck_rent'] . ' '
            . 'WHERE '
            . 'truck_id = "' . $_GET['id'] . '"';
    $result = mysql_query($sql);
    if ($result) {
        echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!")</script>';
        echo '<meta http-equiv="refresh" content="1; URL = truck.php"/>';
        exit();
    } else {
        echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ !!!");window.history.back();</script>';
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>


        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="truck.php">รถบรรทุก</a></li>
                    <li class="active">แก้ไขรถบรรทุก</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">รถบรรทุก</h2>
                </div>
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-pencil"></span> แก้ไขรถบรรทุก</div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3">
                                <form name="truckform" id="truckform" action="" method="post" enctype="multipart/form-data">
                                    <?php
                                    $sql = 'SELECT * FROM truck '
                                            . 'WHERE '
                                            . 'truck.truck_id = "' . $_GET['id'] . '"';
                                    $result = mysql_query($sql);
                                    $row = mysql_fetch_array($result);
                                    ?>
                                    <div class="row">
                                        <div class="col-md-11">
                                            <div class="form-group">
                                                <label>ประเภทรถบรรทุก</label>
                                                <select class="form-control" name="trucktype_id" id="trucktype_id"><?php Trucktype_id($row['trucktype_id']); ?></select>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <p style="margin-top: 28px;">
                                                <a class="btn btn-default btn-sm" href="trucktype.php" title="จัดการประเภทรถบรรทุก"><span class="glyphicon glyphicon-cog"></span></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ทะเบียนเลขที่</label>
                                                <input class="form-control" name="truck_license" type="text" id="truck_license" placeholder="ทะเบียนรถเลขที่ *" value="<?php echo $row['truck_license']; ?>"> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>จังหวัด</label>
                                            <select class="form-control" name="province_id" id="province_id"><?php Province($row['province_id']); ?></select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>เชื้อเพลิง</label>
                                                <select class="form-control" name="fuel_id" id="fuel_id"><?php Fuel_id($row['fuel_id']); ?></select>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <p style="margin-top: 28px;">
                                                <a class="btn btn-default btn-sm" href="fuel.php" title="จัดการเชื้อเพลิง"><span class="glyphicon glyphicon-cog"></span></a>
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <label>สิ้นเปลือง กม./ลิตร</label>
                                            <input class="form-control" name="truck_fueluse" type="number" id="truck_fueluse" placeholder="สิ้นเปลือง กม./ลิตร *" value="<?php echo $row['truck_fueluse']; ?>"> 
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>น้ำหนักบรรทุก (ตัน)</label>
                                            <input class="form-control" name="truck_load" type="number" id="truck_load" placeholder="น้ำหนักบรรทุก (ตัน) *" value="<?php echo $row['truck_load']; ?>"> 
                                        </div>
                                        <div class="col-md-6">
                                            <label>ค่าเช่าต่อเที่ยว</label>
                                            <input class="form-control" name="truck_rent" type="number" id="truck_rent" placeholder="ค่าเช่าต่อเที่ยว *" value="<?php echo $row['truck_rent']; ?>"> 
                                        </div>
                                    </div>
                                    <p>&nbsp;</p>

                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> บันทึก</button>		
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div><!-- /.row -->
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('#truckform').validate({
                    rules: {
                        trucktype_id: {
                            required: true
                        },
                        truck_license: {
                            required: true
                        },
                        province_id: {
                            required: true
                        },
                        fuel_id: {
                            required: true
                        },
                        truck_fueluse: {
                            required: true,
                            number: true,
                            min: 1
                        },
                        truck_load: {
                            required: true,
                            number: true,
                            min: 1
                        },
                        truck_rent: {
                            required: true,
                            number: true,
                            min: 1
                        }
                    },
                    messages: {
                        trucktype_id: {
                            required: 'เลือกประเภทรถบรรทุก'
                        },
                        truck_license: {
                            required: 'กรอกทะเบียนรถ'
                        },
                        province_id: {
                            required: 'เลือกจังหวัด'
                        },
                        fuel_id: {
                            required: 'เลือกเชื้อเพลิงที่ใช้'
                        },
                        truck_fueluse: {
                            required: 'กรอกการสิ้นเปลืองเชื้อเพลิง',
                            number: 'เป็นตัวเลขเท่านั้น',
                            min: 'ต้องมากกว่า 0'
                        },
                        truck_load: {
                            required: 'กรอกน้ำหนักบรรทุก',
                            number: 'เป็นตัวเลขเท่านั้น',
                            min: 'ต้องมากกว่า 0'
                        },
                        truck_rent: {
                            required: 'กรอกค่าเช่าต่อเที่ยว',
                            number: 'เป็นตัวเลขเท่านั้น',
                            min: 'ต้องมากกว่า 0'
                        }
                    }
                });
            });
        </script>
    </body>
</html>
