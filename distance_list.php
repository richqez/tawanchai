<?php
include 'lib/config.php';
include 'lib/conn.php';
include 'lib/function.php';
?>
<script type="text/javascript">
    $(function () {
        $('.usr').click(function () {
            $.post('distance_detail.php', {id: $(this).data('id')},
            function (data) {
                $('#cusdata').html(data);
            });
        });
    });
</script>

<?php
if ($_POST['page']) {
    $page = $_POST['page'];
    $cur_page = $page;
    $page -= 1;
    $per_page = 20;
    $previous_btn = TRUE;
    $next_btn = TRUE;
    $first_btn = TRUE;
    $last_btn = TRUE;
    $start = $page * $per_page;

    $opt = '';

    if ($_POST['search-text'] != '') {
        $opt .= ' AND province.province_name LIKE "%' . $_POST['search-text'] . '%" ';
        echo '<p align="center"><strong>ผลการค้นหา "' . $_POST['search-text'] . '"<br>';
        echo '<a href="">แสดงทั้งหมด</a></strong></p>';
    }
    ?>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="100" class="text-center">ลำดับที่</th>
                    <th class="text-center">ต้นทาง</th>
                    <th class="text-center">จำนวนปลายทาง</th>
                    <th width="100" class="text-center">รายละเอียด</th>
                    <th width="60" class="text-center">แก้ไข</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = 'SELECT *, COUNT(endpoint) AS countend FROM distance, province '
                        . 'WHERE '
                        . 'distance.startpoint = province.province_id ' . $opt
                        . 'GROUP BY distance.startpoint '
                        . 'ORDER BY province.province_name ASC '
                        . 'LIMIT ' . $start . ',' . $per_page;
                $result = mysql_query($sql);
                if (mysql_num_rows($result) == 0) {
                    echo '<tr><td colspan="5" class="text-danger" align="center">ไม่พบข้อมูล</td></tr>';
                } else {
                    $i = 1;
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $i + $start; ?></td>
                            <td><?php echo $row['province_name']; ?></td>
                            <td class="text-center"><?php echo $row['countend']; ?></td>
                            <td class="text-center">
                                <a class="btn btn-info btn-sm usr" title="รายละเอียด" data-toggle="modal" data-target="#myModal" data-id="<?php echo $row['startpoint']; ?>">
                                    <span class="glyphicon glyphicon-th-large"></span>
                                </a> 
                            </td>
                            <td class="text-center">
                                <a class="btn btn-warning btn-sm" href="distance_edit.php?id=<?php echo $row['startpoint']; ?>" title="แก้ไข/เพิ่ม"> <span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
            </tbody>
        </table>
    </div>

    <?php
    $query_pag_num = 'SELECT COUNT(*) as count FROM distance, province '
            . 'WHERE '
            . 'distance.startpoint = province.province_id ' . $opt
            . 'GROUP BY distance.startpoint ';
    $result_pag_num = mysql_query($query_pag_num);
    $row = mysql_fetch_array($result_pag_num);
    $count = $row['count'];
    $no_of_paginations = ceil($count / $per_page);

    include 'lib/pagination/pagination.php';
}