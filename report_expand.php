<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/config.php';
include 'lib/conn.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="js/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="mycss/Mystyle.css" rel="stylesheet">

        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li>รายงาน</li>
                    <li class="active">รายงานรายจ่าย</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">รายงานรายจ่าย</h1>
                </div>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">เลือกเดือนที่ต้องการ</div>
                        <div class="panel-body">
                            <form name="reportform" id="reportform"  method="post" action="report_expand_print.php" target="_blank">
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-2">
                                        <div class="form-group">
                                            <label>ตั้งแต่วันที่</label>
                                            <input type="text" class="form-control" id="_start" name="_start" placeholder="วันที่เริ่มต้น" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ถึงวันที่</label>
                                            <input type="text" class="form-control" id='_end' name="_end" placeholder="วันที่สิ้นสุด" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-md-offset-2">
                                        <div class="form-group">
                                            <label>เลือกรายจ่ายที่ต้องการ</label>
                                            <select class="form-control" id="exselect" name="exselect" required>
                                                <?php
                                                echo '<option value="">เลือกรายจ่ายที่ต้องการ</option>';
                                                foreach ($ex as $k => $v) {
                                                    echo '<option value="' . $k . '">' . $v . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div id="selectdetail"></div>

                                <div class="row">
                                    <div class="col-md-4 col-md-offset-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button type="submit" class="btn btn-primary" name="submit_invoice" id="submit_invoice"><span class="glyphicon glyphicon-check"></span> แสดงผล</button>
                                            <button type="reset" class="btn btn-danger" name="reset" id="reset"><span class="glyphicon glyphicon-refresh"></span> ล้างเงื่อนไข</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div>
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <script src="js/jquery.ui.datepicker.validation.min.js" type="text/javascript"></script>
        <script src="js/datepicker/js/moment-with-locales.js" type="text/javascript"></script>
        <script src="js/datepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script>
            $('#calendar').datepicker({
            });

            !function ($) {
                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function () {
                if ($(window).width() > 768)
                    $('#sidebar-collapse').collapse('show');
            });
            $(window).on('resize', function () {
                if ($(window).width() <= 767)
                    $('#sidebar-collapse').collapse('hide');
            });

            function showDetail() {
                $.post('report_fuel_drv.php', {'exselect': $('#exselect').val()},
                function (data) {
                    $('#selectdetail').show();
                    $('#selectdetail').html(data);
                });
            }

            $(function () {

                $('#exselect').change(function () {
                    if ($('#exselect').val() !== '') {
                        showDetail();
                    } else {
                        $('#selectdetail').hide();
                    }
                });

                $('#reset').click(function () {
                    $('#selectdetail').hide();
                });

                $('#reportform').validate({
                    messages: {
                        _start: {
                            required: 'เลือกวันที่'
                        },
                        _end: {
                            required: 'เลือกวันที่'
                        },
                        exselect: {
                            required: 'เลือกรายจ่ายที่ต้องการ'
                        }
                    }
                });

                $('#_start,#_end').datetimepicker({
                    format: 'D/M/YYYY',
                    locale: 'th'
                });
                $("#_start").on("dp.change", function (e) {
                    $('#_end').data("DateTimePicker").minDate(e.date);
                });
                $("#_end").on("dp.change", function (e) {
                    $('#_start').data("DateTimePicker").maxDate(e.date);
                });
            });
        </script>	
    </body>
</html>
