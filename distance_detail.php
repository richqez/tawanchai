<?php
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/function.php';

$sql = 'SELECT * FROM province '
        . 'WHERE '
        . 'province_id = "' . $_POST['id'] . '" ';
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
?>
<div class="row">
    <div class="col-md-12">
        <p><strong>จังหว้ด : <?php echo $row['province_name']; ?></strong></p>

        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="text-center">ลำดับที่</th>
                        <th class="text-center">ปลายทาง</th>
                        <th class="text-center">ระยะทาง (กม.)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql = 'SELECT * FROM distance, province '
                            . 'WHERE '
                            . 'distance.endpoint = province.province_id AND '
                            . 'distance.startpoint = ' . $_POST['id']
                            . ' ORDER BY province_name asc ';
                    $result = mysql_query($sql);
                    $i = 1;
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $i; ?></td>
                            <td><?php echo $row['province_name']; ?></td>
                            <td class="text-center"><?php echo $row['distance_value']; ?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
