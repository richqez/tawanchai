<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="lib/pagination/style.css" rel="stylesheet" type="text/css"/>
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="invoice.php">ใบแจ้งหนี้</a></li>
                    <li class="active">จัดทำใบแจ้งหนี้ (1. เลือกลูกค้าที่มีค่าใช้จ่าย)</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ใบแจ้งหนี้</h1>
                </div>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-search"></span> เลือกเดือนที่ต้องการจัดทำใบแจ้งหนี้</div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3">
                                <form name="invoiceform" id="invoiceform" action="" method="post">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>เดือน</label>
                                            <select class="form-control" id="invoice_month" name="invoice_month" required>
                                                <?php
                                                $month = array(
                                                    1 => "มกราคม",
                                                    "กุมภาพันธ์",
                                                    "มีนาคม",
                                                    "เมษายน",
                                                    "พฤษภาคม",
                                                    "มิถุนายน",
                                                    "กรกฎาคม",
                                                    "สิงหาคม",
                                                    "กันยายน",
                                                    "ตุลาคม",
                                                    "พฤศจิกายน",
                                                    "ธันวาคม"
                                                );
                                                echo '<option value="">เลือกเดือน</option>';
                                                foreach ($month as $k => $v) {
                                                    echo '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ปี</label>
                                            <select class="form-control" id="invoice_year" name="invoice_year" required>
                                                <?php
                                                echo '<option value="">เลือกปี</option>';
                                                for ($y = date('Y') - 1; $y <= date('Y') + 3; $y++) {
                                                    echo '<option value="' . $y . '" ' . $selected . '>' . ($y + 543) . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button type="submit" class="btn btn-primary" name="submit_invoice" id="submit_invoice"><span class="glyphicon glyphicon-check"></span> แสดงรายชื่อลูกค้า</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div>

            <?php
//            if (isset($_POST['submit_invoice'])) {
            ?>
            <div id="containerdata">
                <div id="loading" class="text-center"></div>
                <div class="data"></div>
                <div class="pagination"></div>
            </div>
            <?php // } ?>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">รายละเอียดลูกค้า</h4>
                        </div>
                        <div class="modal-body">                            
                            <div id="cusdata"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-table.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <script type="text/javascript">
            !function ($) {
                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function () {
                if ($(window).width() > 768)
                    $('#sidebar-collapse').collapse('show');
            });
            $(window).on('resize', function () {
                if ($(window).width() <= 767)
                    $('#sidebar-collapse').collapse('hide');
            });

            $(function () {
                $('#invoiceform').validate({
                    messages: {
                        invoice_month: {
                            required: 'เลือกเดือน'
                        },
                        invoice_year: {
                            required: 'เลือกปี'
                        }
                    }
                });

                function loading_show() {
                    $('#loading').html("<img src='images/ajax-loader.gif'/>").fadeIn('fast');
                }

                function loading_hide() {
                    $('#loading').fadeOut('slow');
                }

                function loadData(numpage) {
                    loading_show();
                    $.ajax({
                        url: "invoice_cus_list.php",
                        type: 'post',
                        data: {
                            page: numpage, 'invoice_month': $('#invoice_month').val(), 'invoice_year': $('#invoice_year').val()
                        },
                        success: function (data) {
                            loading_hide();
                            $("#containerdata").html(data);
                            
                        }
                    });
                }

                $('#submit_invoice').click(function (event) {
                    loadData(1);
                    event.preventDefault();
                });

                loadData(1);

                $('#containerdata .pagination li.active').live('click', function () {
                    var page = $(this).attr('p');
                    loadData(page);

                });
            });
        </script>
    </body>
</html>
