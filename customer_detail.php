<?php
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/function.php';
?>
<div class="row">
    <?php
    $sql = 'SELECT * FROM customer '
            . 'WHERE '
            . 'cus_id = "' . $_POST['id'] . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    ?>

    <div class="row">
        <div class="col-md-4 col-md-offset-2"><p><strong>รหัสลูกค้า :</strong></p></div>
        <div class="col-xs-6"><p><?php echo $row['cus_id']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-2"><p><strong>ชื่อบริษัท :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['cus_name']; ?></p></div>
    </div>
    
    <div class="row">
        <div class="col-md-4 col-md-offset-2"><p><strong>ประกอบธุรกิจเกี่ยวกับ :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['cus_about']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-2"><p><strong>ที่อยู่ :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['cus_address']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-2"><p><strong>เบอร์โทร :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['cus_tel']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-2"><p><strong>อีเมล์ :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['cus_email']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-2"><p><strong>Line ID :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['cus_line']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-2"><p><strong>ผู้ประสานงาน :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['cus_contact']; ?></p></div>
    </div>
    
    <div class="row">
        <div class="col-md-4 col-md-offset-2"><p><strong>วัน/เวลา ที่เพิ่ม :</strong><p></div>
        <div class="col-xs-6"><p><?php echo DatetimeThai($row['cus_registime']); ?></p></div>
    </div>

</div>
