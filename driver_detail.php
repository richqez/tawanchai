<?php
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/function.php';
?>
<div class="row">
    <?php
    $sql = 'SELECT * FROM driver, truck, trucktype, province '
            . 'WHERE '
            . 'driver.truck_id = truck.truck_id AND '
            . 'truck.trucktype_id = trucktype.trucktype_id AND '
            . 'truck.province_id = province.province_id AND '
            . 'driver.drv_id = "' . $_POST['id'] . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    ?>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"></div>
        <div class="col-xs-6">
            <p>
                <img src="images/driver/<?php echo $row['drv_image']; ?>" alt="<?php echo $row['drv_id']; ?>" title="<?php echo $row['drv_id']; ?>" class="img-thumbnail">
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>รหัสพนักงานขับรถ :</strong></p></div>
        <div class="col-xs-6"><p><?php echo $row['drv_id']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>ชื่อ-นามสกุล :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['drv_name']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>เลขประจำตัวประชาชน :</strong><p></div>
        <div class="col-xs-6"><p><?php echo DisplayCardID($row['drv_idcard']); ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>เลขที่ใบขับขี่ :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['drv_licensecar']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>วัน-เดือน-ปี เกิด :</strong><p></div>
        <div class="col-xs-6"><p><?php echo ThaidateNoTime($row['drv_birthday']); ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>ที่อยู่ :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['drv_address']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>เบอร์โทร :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['drv_tel']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>อีเมล์ :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['drv_email']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>ทะเบียนรถ :</strong><p></div>
        <div class="col-xs-6">
            <p>  
                <?php
                if ($row['drv_status'] == '0') {
                    echo '-';
                } else {
                    echo $row['truck_license'] . '<br>' . $row['province_name'];
                }
                ?>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>วัน/เวลา ที่เพิ่ม :</strong><p></div>
        <div class="col-xs-6"><p><?php echo DatetimeThai($row['drv_registime']); ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>สถานะ :</strong></p></div>
        <div class="col-xs-6"><p><?php echo $user_status[$row['drv_status']]; ?></p></div>
    </div>

</div>
