<?php
session_start();
if (isset($_SESSION['emp_id'])) {
    header('Location: index.php');
}
include 'lib/config.php';
include 'lib/conn.php';
if (isset($_POST['submit'])) {
    $sql = 'SELECT * FROM employee '
            . 'WHERE '
            . 'emp_username = "' . $_POST['username'] . '" AND '
            . 'emp_password = "' . md5($_POST['password']) . '" AND '
            . 'emp_status = "1"';
    $result = mysql_query($sql);
    if (mysql_num_rows($result) == 0) {
        echo '<script>alert("ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง !!!");window.history.back();</script>';
        exit();
    } else {
        $row = mysql_fetch_array($result);
        $_SESSION['emp_username'] = $row['emp_username'];
        $_SESSION['emp_id'] = $row['emp_id'];
        $_SESSION['emp_name'] = $row['emp_name'];
        $_SESSION['emp_image'] = $row['emp_image'];
        $_SESSION['emp_type'] = $row['emp_type'];
        echo '<meta http-equiv="refresh" content="1; URL = index.php"/>';
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading"><?php echo SYS_NAME; ?></div>
                    <div class="panel-body">
                        <form role="form" name="FormLogin" id="FormLogin" method="post" action="">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="ชื่อผู้ใช้ *" name="username" type="text" autofocus="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="รหัสผ่าน *" name="password" type="password" value="">
                                </div>
                                <input type="submit" class="btn btn-primary" name="submit" value="เข้าสู่ระบบ">
                                <input type="reset" class="btn btn-danger" name="reset" value="ยกเลิก">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div><!-- /.col-->
        </div><!-- /.row -->	

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script>
            !function ($) {
                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function () {
                if ($(window).width() > 768)
                    $('#sidebar-collapse').collapse('show')
            })
            $(window).on('resize', function () {
                if ($(window).width() <= 767)
                    $('#sidebar-collapse').collapse('hide')
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#FormLogin').validate({
                    rules: {
                        username: {
                            required: true
                        },
                        password: {
                            required: true
                        }
                    },
                    messages: {
                        username: {
                            required: 'กรอกชื่อผู้ใช้'
                        },
                        password: {
                            required: 'กรอกรหัสผ่าน'
                        }
                    }
                });
            });
        </script>
    </body>
</html>
