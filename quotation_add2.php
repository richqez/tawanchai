<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';

$sql = 'SELECT * FROM invoice '
        . 'WHERE '
        . 'invoice_month = "' . (int) date('m') . '" '
        . 'AND invoice_year = "' . date('Y') . '" '
        . 'AND cus_id = "' . $_GET['id'] . '"';
$recheck = mysql_query($sql);
if (mysql_num_rows($recheck) > 0) {
    echo '<meta charset="utf-8">';
    echo '<script>alert("ลูกค้ารายได้ออกใบแจ้งหนี้แล้ว ไม่สามารถจัดทำใบเสนอราคาได้ !!!");</script>';
    echo '<meta http-equiv="refresh" content="1; URL = quotation_add.php"/>';
    exit();
}

if (!isset($_GET['id']) && !isset($_SESSION['cus_id'])) {
    echo '<meta http-equiv="refresh" content="1; URL = quotation.php"/>';
    exit();
} else if (!isset($_SESSION['cus_id'])) {
    $_SESSION['cus_id'] = $_GET['id'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="js/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="js/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="lib/pagination/style.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="quotation.php">ใบเสนอราคา</a></li>
                    <li class="active">จัดทำใบเสนอราคา (2. กำหนดรายละเอียด)</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ใบเสนอราคา</h1>
                </div>
            </div><!--/.row-->

            <form name="quotationform" id="quotationform" action="quotation_preview.php" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">ใบเสนอราคา</div>
                            <div class="panel-body">                            
                                <div class="row">
                                    <div class="col-md-6">
                                        <input name="cus_id" type="hidden" id="cus_id" value="<?php echo $_SESSION['cus_id']; ?>">
                                        <?php
                                        $sql = 'SELECT * FROM customer WHERE cus_id = "' . $_SESSION['cus_id'] . '"';
                                        $result = mysql_query($sql);
                                        $row = mysql_fetch_array($result);
                                        ?>
                                        <p><strong>รหัสลูกค้า : </strong><?php echo $row['cus_id']; ?></p>
                                        <p><strong>ชื่อบริษัท : </strong><?php echo $row['cus_name']; ?></p>
                                        <p><strong>ที่อยู่ : </strong><?php echo $row['cus_address']; ?></p>
                                        <p><strong>เบอร์โทร : </strong><?php echo $row['cus_tel']; ?></p>
                                        <p><strong>E-mail : </strong><?php echo $row['cus_email']; ?> <strong> Line ID : </strong><?php echo $row['cus_line']; ?></p>
                                        <p><strong>ผู้ประสานงาน : </strong><?php echo $row['cus_contact']; ?></p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <p><strong>เลขที่ : </strong><?php echo NextQuo_id(); ?></p>
                                        <p><strong>วันที่ : </strong><?php echo ThaidateNoTime(date('Y-m-d')); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.col-->
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">ข้อมูลปลายทาง</div>
                            <div class="panel-body">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="form-group">
                                        <label>ชื่อ-นามสกุล</label>
                                        <input class="form-control" name="quo_name" type="text" id="quo_name" placeholder="ชื่อ-นามสกุล *" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_name'] : ''; ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <label>ที่อยู่</label>
                                        <textarea class="form-control" name="quo_address" id="quo_address" placeholder="ที่อยู่ *" rows="5"><?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_address'] : ''; ?></textarea>    
                                    </div>
                                    <div class="form-group">
                                        <label>เบอร์โทร</label>
                                        <input class="form-control" name="quo_tel" type="text" id="quo_tel" placeholder="เบอร์โทรศัพท์ *" maxlength="10" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_tel'] : ''; ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <label>อีเมล์</label>
                                        <input class="form-control" name="quo_mail" type="email" id="quo_mail" placeholder="อีเมล์ *" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_mail'] : ''; ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <label>Line ID</label>
                                        <input class="form-control" name="quo_line" type="text" id="quo_line" placeholder="Line ID" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_line'] : ''; ?>"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.col-->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">รายละเอียด</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>รายละเอียดสินค้า</label>
                                            <textarea class="form-control" name="quo_product" id="quo_product" placeholder="รายละเอียดสินคัา *" rows="3"><?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_product'] : ''; ?></textarea>    
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>จำนวน</label>
                                                    <input class="form-control" name="quo_count" type="number" id="quo_count" placeholder="จำนวน" min="1" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_count'] : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>หน่วย</label>
                                                    <select class="form-control" name="quo_unit" id="quo_unit"><?php Unit((isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_unit'] : ''); ?></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ต้นทาง</label>
                                                    <select class="form-control" name="startpoint" id="startpoint"><?php Startpoint((isset($_SESSION['quo'])) ? $_SESSION['quo']['startpoint'] : ''); ?></select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ปลายทาง</label>
                                                    <select class="form-control" name="endpoint" id="endpoint"></select>
                                                    <input type="hidden" name="s_endpoint" id="s_endpoint" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['endpoint'] : ''; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ระยะทาง</label>
                                                    <input class="form-control" name="quo_distance" type="number" id="quo_distance" placeholder="ระยะทาง" min="1" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_distance'] : ''; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p style="margin-top: 28px;"><strong>กิโลเมตร</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>วันที่เริ่มต้น</label>
                                                    <input class="form-control" name="quo_startdate" type="text" id="quo_startdate" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_startdate'] : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>วันที่สิ้นสุด</label>
                                                    <input class="form-control" name="quo_enddate" type="text" id="quo_enddate" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_enddate'] : ''; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>จำนวนเที่ยว</label>
                                                    <input class="form-control" name="quo_number" type="number" id="quo_number" placeholder="จำนวนเที่ยว" min="1" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_number'] : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p style="margin-top: 28px;"><strong>เที่ยว</strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>ประเภทรถบรรทุก</label>
                                                <select class="form-control" id="trucktype_id" name="trucktype_id">
                                                    <?php Trucktype_id((isset($_SESSION['quo'])) ? $_SESSION['quo']['trucktype_id'] : ''); ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>ทะเบียนรถบรรทุก</label>
                                                <select class="form-control" id="truck_id" name="truck_id"></select>
                                                <input type="hidden" name="s_truck_id" id="s_truck_id" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['truck_id'] : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <br>
                                            <label>พนักงานขับรถ</label>
                                            <input class="form-control" name="drv_name" type="text" id="drv_name" placeholder="พนักงานขับรถ" readonly value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['drv_name'] : ''; ?>">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>น้ำมันเเชื้อเพลิง</label>
                                                    <input class="form-control" name="fuel_name" type="text" id="fuel_name" placeholder="น้ำมันเชื้อเพลิง" readonly value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['fuel_name'] : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ลิตรละ</label>
                                                    <input class="form-control" name="quo_fuel" type="number" min="0" id="quo_fuel" placeholder="0" value="<?php echo (isset($_SESSION['quo'])) ? $_SESSION['quo']['quo_fuel'] : ''; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <hr>
                                    <div class="col-md-2 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> ตัวอย่างใบเสนอราคา</button>		
                                    </div>
                                    <div class="col-md-2">
                                        <a class="btn btn-danger" href="quotation_cancel.php" title="ยกเลิก" onclick="return confirm('ยกเลิกรายการนี้หรือไม่ ?');"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.col-->
                </div>

            </form>
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-table.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <script src="js/jquery-ui.js" type="text/javascript"></script>
        <script src="js/jquery.ui.datepicker.validation.min.js" type="text/javascript"></script>
        <script src="js/datepicker/js/moment-with-locales.js" type="text/javascript"></script>        
        <script src="js/datepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script type="text/javascript">
                                            !function ($) {
                                                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                                                    $(this).find('em:first').toggleClass("glyphicon-minus");
                                                });
                                                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
                                            }(window.jQuery);

                                            $(window).on('resize', function () {
                                                if ($(window).width() > 768)
                                                    $('#sidebar-collapse').collapse('show');
                                            });
                                            $(window).on('resize', function () {
                                                if ($(window).width() <= 767)
                                                    $('#sidebar-collapse').collapse('hide');
                                            });


                                            $(function () {
                                                jQuery.validator.addMethod("greaterThan",
                                                        function (value, element, params) {
                                                            if (!/Invalid|NaN/.test(new Date(value))) {
                                                                return new Date(value) > new Date($(params).val());
                                                            }

                                                            return isNaN(value) && isNaN($(params).val())
                                                                    || (Number(value) > Number($(params).val()));
                                                        }, 'เลือกวันที่ย้อนหลัง.');
                                                Truck_id();
                                                $('#trucktype_id').change(function () {
                                                    Truck_id();
                                                    $('#drv_name').val("");
                                                    $('#drv_id').val("");
                                                    $('#fuel_name').val("");
                                                });
                                                $('#truck_id').change(function () {
                                                    Driver_name();
                                                    Fuel_name();
                                                });

                                                Endpoint();
                                                $('#startpoint').change(function () {
                                                    Endpoint();
                                                    Distance();
                                                });

                                                $('#endpoint').change(function () {
                                                    Distance();
                                                });

                                                $('#quotationform').validate({
                                                    rules: {
                                                        quo_product: {
                                                            required: true
                                                        },
                                                        quo_count: {
                                                            required: true,
                                                            number: true,
                                                            min: 1
                                                        },
                                                        quo_unit: {
                                                            required: true
                                                        },
                                                        startpoint: {
                                                            required: true
                                                        },
                                                        endpoint: {
                                                            required: true
                                                        },
                                                        quo_distance: {
                                                            required: true,
                                                            number: true,
                                                            min: 1
                                                        },
                                                        quo_startdate: {
                                                            required: true
                                                        },
                                                        quo_enddate: {
                                                            required: true,
                                                            greaterThan: '#quo_startdate'
                                                        },
                                                        quo_number: {
                                                            required: true,
                                                            number: true,
                                                            min: 1
                                                        },
                                                        trucktype_id: {
                                                            required: true
                                                        },
                                                        truck_id: {
                                                            required: true
                                                        },
                                                        drv_name: {
                                                            required: true
                                                        },
                                                        quo_fuel: {
                                                            required: true,
                                                            number: true,
                                                            min: 1
                                                        },
                                                        quo_name: {
                                                            required: true
                                                        },
                                                        quo_address: {
                                                            required: true
                                                        },
                                                        quo_tel: {
                                                            required: true
                                                        },
                                                        quo_mail: {
                                                            required: true,
                                                            email: true
                                                        }
                                                    },
                                                    messages: {
                                                        quo_product: {
                                                            required: 'กรอกรายละเอียดสินค้า'
                                                        },
                                                        quo_count: {
                                                            required: 'กรอกจำนวน',
                                                            number: 'เป็นตัวเลขเท่านั้น',
                                                            min: 'อย่างน้อย 1'
                                                        },
                                                        quo_unit: {
                                                            required: 'เลือกหน่วย'
                                                        },
                                                        startpoint: {
                                                            required: 'เลือกจังหวัดต้นทาง'
                                                        },
                                                        endpoint: {
                                                            required: 'เลือกจังหวัดปลายทาง'
                                                        },
                                                        quo_distance: {
                                                            required: 'เลือกต้นทาง-ปลายทาง',
                                                            number: 'เป็นตัวเลขเท่านั้น',
                                                            min: 'อย่างน้อย 1'
                                                        },
                                                        quo_startdate: {
                                                            required: 'เลือกวันที่เริ่มต้น'
                                                        },
                                                        quo_enddate: {
                                                            required: 'เลือกวันที่สิ้นสุด'
                                                        },
                                                        quo_number: {
                                                            required: 'กรอกจำนวนเที่ยว',
                                                            number: 'เป็นคัวเลขเท่านั้น',
                                                            min: 'อย่างน้อย 1'
                                                        },
                                                        trucktype_id: {
                                                            required: 'เลือกประเภทรถบรรทุก'
                                                        },
                                                        truck_id: {
                                                            required: 'เลือกรถบรรทุก'
                                                        },
                                                        drv_name: {
                                                            required: 'เลือกประเภทรถบรรทุก และรถบรรทุก'
                                                        },
                                                        quo_fuel: {
                                                            required: 'กรอกราคาน้ำมันเชื้อเพลิง',
                                                            number: 'เป็นคัวเลขเท่านั้น',
                                                            min: 'อย่างน้อย 1'
                                                        },
                                                        quo_name: {
                                                            required: 'กรอกชื่อ-นามสกุล'
                                                        },
                                                        quo_address: {
                                                            required: 'กรอกที่อยู่'
                                                        },
                                                        quo_tel: {
                                                            required: 'กรอกเบอร์โทร'
                                                        },
                                                        quo_mail: {
                                                            required: 'กรอก email',
                                                            email: 'รูปแบบ e-mail ไม่ถูกต้อง'
                                                        }
                                                    }
                                                });
                                            });


                                            function Truck_id() {
                                                $.post('lib/truck_id_quo.php', {'trucktype_id': $('#trucktype_id').val(), truck_id: $('#s_truck_id').val()},
                                                function (data) {
                                                    $('#truck_id').html(data);
                                                });
                                            }

                                            function Driver_name() {
                                                $.post('lib/driver_name.php', {'truck_id': $('#truck_id').val()},
                                                function (data) {
                                                    $('#drv_name').val(data);
                                                });
                                            }

                                            function Fuel_name() {
                                                $.post('lib/fuel_name.php', {'truck_id': $('#truck_id').val()},
                                                function (data) {
                                                    $('#fuel_name').val(data);
                                                });
                                            }

                                            function Endpoint() {
                                                $.post('lib/endpoint.php', {startpoint: $('#startpoint').val(), endpoint: $('#s_endpoint').val()},
                                                function (data) {
                                                    $('#endpoint').html(data);
                                                });
                                            }

                                            function Distance() {
                                                $.post('lib/distance.php', {startpoint: $('#startpoint').val(), endpoint: $('#endpoint').val()},
                                                function (data) {
                                                    $('#quo_distance').val(data);
                                                });
                                            }

        </script>
        <script>
            $(function () {
                $('#quo_startdate,#quo_enddate').datetimepicker({
                    format: 'D/M/YYYY',
                    locale: 'th'
                    <?php echo (isset($_SESSION['quo'])) ? '' : ',minDate: moment()'; ?>
                });
                $("#quo_startdate").on("dp.change", function (e) {
                    $('#quo_enddate').data("DateTimePicker").minDate(e.date);
                });
                $("#quo_enddate").on("dp.change", function (e) {
                    $('#quo_startdate').data("DateTimePicker").maxDate(e.date);
                });
            });
        </script>
    </body>
</html>
