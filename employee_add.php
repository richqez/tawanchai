<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/class.upload.php';

if (isset($_POST['submit'])) {
    echo '<meta charset="utf-8">';
    $sql = 'SELECT * FROM employee WHERE emp_email = "' . $_POST['emp_email'] . '"';
    $recheack = mysql_query($sql);
    if (mysql_num_rows($recheack) > 0) {
        echo '<script>alert("อีเมล์นี้ มีผู้ใช้งานแล้ว !!!");window.history.back();</script>';
        exit();
    }
    $emp_id = NextEmp_id();
    echo '<meta charset="utf-8">';
    if ($_FILES['emp_image']['name'] == '') {
        $image_name = 'human.jpg';
    } else {
        $upload_image = new upload($_FILES['emp_image']);
        if ($upload_image->uploaded) {
            $upload_image->image_resize = true;
            $upload_image->image_x = 100;
            $upload_image->image_ratio_y = true;
            $upload_image->file_new_name_body = $emp_id;
            $upload_image->process("images/employee");

            if ($upload_image->processed) {
                $image_name = $upload_image->file_dst_name;
                $upload_image->clean();
            } else {
                echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกรูปภาพได้ !!!");window.history.back();</script>';
                exit();
            }
        }
    }
    $sql = 'INSERT INTO employee '
            . '(emp_id, emp_username, emp_password, emp_name, emp_address, '
            . 'emp_tel, emp_email, emp_registime, emp_status, emp_type, emp_image)'
            . 'VALUES '
            . '("' . $emp_id . '", '
            . '"' . trim($_POST['emp_username']) . '", '
            . '"' . md5('password') . '", '
            . '"' . trim($_POST['emp_name']) . '", '
            . '"' . trim($_POST['emp_address']) . '", '
            . '"' . trim($_POST['emp_tel']) . '", '
            . '"' . trim($_POST['emp_email']) . '", '
            . 'NOW(), '
            . '"1", "' . $_POST['emp_type'] . '",'
            . '"' . $image_name . '") ';
    $result = mysql_query($sql);
    if ($result) {
        echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!")</script>';
        echo '<meta http-equiv="refresh" content="1; URL = employee.php"/>';
        exit();
    } else {
        echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ !!!");window.history.back();</script>';
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>


        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="employee.php">พนักงาน</a></li>
                    <li class="active">เพิ่มพนักงาน</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">พนักงาน</h2>
                </div>
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-plus"></span> เพิ่มพนักงาน</div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3">
                                <form name="employeeform" id="employeeform" action="" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>ชื่อผู้ใช้</label>
                                        <input class="form-control" name="emp_username" type="text" id="emp_username" placeholder="ชื่อผู้ใช้ *">
                                    </div>
                                    <div class="form-group">
                                        <label>ชื่อ-นามสกุล</label>
                                        <input class="form-control" name="emp_name" type="text" id="emp_name" placeholder="ชื่อ-นามสกุล *"> 
                                    </div>
                                    <div class="form-group">
                                        <label>ที่อยู่</label>
                                        <textarea class="form-control" name="emp_address" id="emp_address" placeholder="ที่อยู่ *" rows="5"></textarea>    
                                    </div>
                                    <div class="form-group">
                                        <label>เบอร์โทรศัพท์</label>
                                        <input class="form-control" name="emp_tel" type="text" id="emp_tel" placeholder="เบอร์โทรศัพท์ *" maxlength="10"> 
                                    </div>
                                    <div class="form-group">
                                        <label>อีเมล์</label>
                                        <input class="form-control" name="emp_email" type="email" id="emp_email" placeholder="อีเมล์ *"> 
                                    </div>

                                    <div class="form-group">
                                        <label>รูปภาพ</label>
                                        <input type="file" class="form-control" id="emp_image" name="emp_image" placeholder="รูปภาพ">
                                    </div>

                                    <div class="form-group">
                                        <label>ประเภทผู้ใช้</label>
                                        <select class="form-control" id="emp_type" name="emp_type">
                                            <?php Employeetype(); ?>
                                        </select>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> บันทึก</button>		
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div><!-- /.row -->
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $.validator.addMethod("lettersonly", function (value, element, param) {
                    return value.match(new RegExp("." + param + "$"));
                });

                $('#employeeform').validate({
                    rules: {
                        emp_username: {
                            required: true,
                            minlength: 4,
                            maxlength: 12,
                            lettersonly: "[a-zA-Z]+"
                        },
                        emp_name: {
                            required: true
                        },
                        emp_address: {
                            required: true
                        },
                        emp_tel: {
                            required: true,
                            number: true,
                            minlength: 10
                        },
                        emp_email: {
                            required: true,
                            email: true
                        },
                        emp_type: {
                            required: true
                        },
                        emp_image: {
                            accept: "jpeg|png|jpg|bmp"
                        }
                    },
                    messages: {
                        emp_username: {
                            required: 'กรอกชื่อผู้ใช้',
                            minlength: 'ไม่ต่ำกว่า 4 ตัวอักษร',
                            maxlength: 'ไม่เกิน 12 ตัวอักษร',
                            lettersonly: 'เป็นตัวอักษรภาษาอังกฤษเท่านั้น'
                        },
                        emp_name: {
                            required: 'กรอกชื่อ-นามสกุล'
                        },
                        emp_address: {
                            required: 'กรอกที่อยู่'
                        },
                        emp_tel: {
                            required: 'กรอกเบอร์โทรศัพท์',
                            number: 'เป็นตัวเลขเท่านั้น',
                            minlength: 'ต้องเป็น 10 หลัก'
                        },
                        emp_email: {
                            required: 'กรอกอีเมล์',
                            email: 'รูปแบบอีเมล์ไม่ถูกต้อง'
                        },
                        emp_type: {
                            required: 'เลือกประเภทพนักงาน'
                        },
                        emp_image: {
                            accept: 'ต้องเป็นไฟล์รูปภาพเท่านั้น (.jpg,.png,.jpeg,.bmp)'
                        }
                    }
                });
            });
        </script>
    </body>
</html>
