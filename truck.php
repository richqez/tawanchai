<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="lib/pagination/style.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li class="active">รถบรรทุก</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">รถบรรทุก</h1>
                </div>
            </div><!--/.row-->

            <div class="row">
                <div class="col-md-3">
                    <button class="btn btn-default" type="button" onclick="window.location.href = 'truck_add.php'"> <span class="glyphicon glyphicon-plus"></span> เพิ่มรถบรรทุก</button>
                    <p>&nbsp;</p>
                </div>      
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="input-group">
                        <input type="text" class="form-control" id="search-text" placeholder="พิมพ์ค้นหา...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" id="btnsearch" type="submit"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                        </span>
                    </div>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-default" type="button" onclick="window.location.href = ''">แสดงทั้งหมด</button>
                </div>      
                <div class=" col-md-3 text-right">
                    <select class="form-control" name="trucktype_id" id="trucktype_id">
                        <?php Trucktype_id(); ?>
                    </select>
                </div>      
                <div class=" col-md-2 text-right">
                    <select class="form-control" name="fuel_id" id="fuel_id">
                        <?php Fuel_id(); ?>
                    </select>
                </div>      
            </div>
            <p>&nbsp;</p>
            <div id="containerdata">
                <div id="loading" class="text-center"></div>
                <div class="data"></div>
                <div class="pagination"></div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">รายละเอียดรถบรรทุก</h4>
                        </div>
                        <div class="modal-body">                            
                            <div id="truckdata"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-table.js"></script>
        <script type="text/javascript">
                        !function ($) {
                            $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                                $(this).find('em:first').toggleClass("glyphicon-minus");
                            });
                            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
                        }(window.jQuery);

                        $(window).on('resize', function () {
                            if ($(window).width() > 768)
                                $('#sidebar-collapse').collapse('show');
                        });
                        $(window).on('resize', function () {
                            if ($(window).width() <= 767)
                                $('#sidebar-collapse').collapse('hide');
                        });

                        $(function () {

                            function loading_show() {
                                $('#loading').html("<img src='images/ajax-loader.gif'/>").fadeIn('fast');
                            }

                            function loading_hide() {
                                $('#loading').fadeOut('slow');
                            }

                            function loadData(numpage) {
                                loading_show();
                                $.ajax({
                                    url: "truck_list.php",
                                    type: 'post',
                                    data: {
                                        page: numpage,
                                        'search-text': $('#search-text').val(),
                                        trucktype_id: $('#trucktype_id').val(),
                                        fuel_id: $('#fuel_id').val()
                                    },
                                    success: function (data) {
                                        loading_hide();
                                        $("#containerdata").html(data);
                                    }
                                });
                                $('#search-text').attr('value', '');
                            }

                            $('#btnsearch').click(function () {
                                loadData(1);
                            });

                            loadData(1);

                            $('#trucktype_id').change(function () {
                                loadData(1);
                            });

                            $('#fuel_id').change(function () {
                                loadData(1);
                            });

                            $('#containerdata .pagination li.active').live('click', function () {
                                var page = $(this).attr('p');
                                loadData(page);

                            });
                        });
        </script>
    </body>
</html>
