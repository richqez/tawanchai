<?php
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/function.php';

$htmlh = '<h2 align="center">รายงานรายจ่าย</h2>';
$htmlh .='<h2 align="center">'.$ex[$_POST['exselect']].'</h2>';
$htmlh .='<h2 align="center">ประจำวันที่ '.ThaidateNoTime2($_POST['_start']).' ถึงวันที่ '.ThaidateNoTime2($_POST['_end']).' </h2>';
$sql = 'SELECT * FROM quotation, customer '
                . 'WHERE '
                . 'quotation.cus_id = customer.cus_id AND '
                . 'quo_status = "1" AND '
                . '(quo_date BETWEEN "' . DateFormatDB($_POST['_start']) . '" AND "' . DateFormatDB($_POST['_end']) . '" ) ';
$result = mysql_query($sql);
$html ='';
        if (mysql_num_rows($result) == 0) {
          
            $html .='<h3 align="center">=== ไม่มีข้อมูล ===</h3>';
            
        } else {

            if ($_POST['exselect'] == 1) {
                $opt = '';

                if ($_POST['fuel_id'] != '') {
                    $opt = ' AND truck.fuel_id = "' . $_POST['fuel_id'] . '" ';
                }
            
            $sql = 'SELECT * FROM quotation, customer, truck '
                        . 'WHERE '
                        . 'quotation.cus_id = customer.cus_id AND quotation.truck_id = truck.truck_id AND '
                        . 'quo_status = "1" AND '
                        . '(quo_date BETWEEN "' . DateFormatDB($_POST['_start']) . '" AND "' . DateFormatDB($_POST['_end']) . '" ) ' . $opt;
            $result = mysql_query($sql);

            $total = 0;
            $i = 1;
            $html .= '<table width="90%" align="center" border="1" style="line-height:2em; border-collapse: collapse;">';
            $html .= '<tr>';       
            $html .= '<th height="45">ลำดับที่</th>';           
            $html .= '<th>ใบเสนอราคาเลขที่</th>';           
            $html .= '<th>ลงวันที่</th>';           
            $html .= '<th>ประเภทเชื้อเพลิง</th>';           
            $html .= ' <th>ลิตรละ</th>';          
            $html .= '<th>ปริมาณ</th>';           
            $html .= '<th>จำนวนเงิน (บาท)</th>';           
            $html .= '</tr>';       
           

            while ($row = mysql_fetch_array($result)) {
                        $total += $row['quo_fuel_total'];
                        
                        $html .='<tr>';
                        $html .='<td height="40" align="center">'.$i.'</td>';   
                        $html .='<td align="center">'. $row['quo_id'].'</td>';   
                        $html .='<td align="center">'.ThaidateNoTime($row['quo_date']).'</td>';   
                        $html .='<td align="left">'. Fuel_Name($row['fuel_id']).'</td>';   
                        $html .='<td align="center">'. number_format($row['quo_fuel_price'], 2).'</td>';   
                        $html .=' <td align="center">'. number_format($row['quo_fuel_value']).'</td>';  
                        $html .=' <td align="right">'. number_format($row['quo_fuel_total'], 2).'</td>';   
                        $html .='</tr>';
                       
                        $i++;
                    }
            $html .='<tr>';
            $html .='<td height="40" colspan="6" align="right"><strong>รวมทั้งสิ้น&nbsp;&nbsp;  </strong></td>';
            $html .='<td align="right"><strong>'.number_format($total, 2).'</strong></td>';
            $html .='</tr>';
            $html .='</table>';
        } else if ($_POST['exselect'] == 2) {
            $opt = '';

            if ($_POST['drv_id'] != '') {
                $opt = ' AND drv_id = "' . $_POST['drv_id'] . '" ';
            }

            $sql = 'SELECT * FROM quotation, customer '
                        . 'WHERE '
                        . 'quotation.cus_id = customer.cus_id AND '
                        . 'quo_status = "1" AND '
                        . '(quo_date BETWEEN "' . DateFormatDB($_POST['_start']) . '" AND "' . DateFormatDB($_POST['_end']) . '" ) ' . $opt;
            $result = mysql_query($sql);

            $total = 0;
            $i = 1; 

            $html .= '<table width="90%" align="center" border="1" style="line-height:2em; border-collapse: collapse;">';
            $html .= '<tr>';       
            $html .= '<th height="45">ลำดับที่</th>';           
            $html .= '<th>ใบเสนอราคาเลขที่</th>';           
            $html .= '<th>ลงวันที่</th>';           
            $html .= '<th>พนักงานขับรถ</th>';           
            $html .= ' <th>ส่วนแบ่ง (บาท)</th>';                   
            $html .= '</tr>';  
             
            while ($row = mysql_fetch_array($result)) {
                $total += $row['quo_drv_per'];
                $html .= '<tr>';
                $html .= '<td height="40" align="center"> '.$i.'</td>';
                $html .= '<td align="center"> '.$row['quo_id'].'</td>';
                $html .= '<td align="center">'. ThaidateNoTime($row['quo_date']).'</td>';
                $html .= '<td align="left"> '.Getdriver2($row['drv_id']).'</td>';
                $html .= '<td align="right"> '.number_format($row['quo_drv_per'], 2).'</td>';
                $html .= '</tr>';
                   
                $i++;
            }  
            $html .='<tr>';
            $html .='<td height="40" colspan="4" align="right"><strong>รวมทั้งสิ้น&nbsp;&nbsp;</strong></td>';
            $html .='<td align="right"><strong>'.number_format($total, 2).'</strong></td>';
            $html .='</tr>';
            $html .='</table>';     
        }
    }






include("lib/mpdf/mpdf.php");
$mpdf = new mPDF('UTF-8');
$mpdf = new mPDF('th',  'A4-L', '', 'angsanaupc');
// $mpdf = new mPDF('', 'A4', '', '');
$mpdf->SetHTMLHeader($htmlh);
$mpdf->setFooter("หน้า {PAGENO} of {nb}");
$mpdf->PDFAauto = true;
$mpdf->SetTopMargin(50);
$mpdf->SetAutoFont();
// $mpdf->SetAutoPageBreak();
$mpdf->WriteHTML($html);
$mpdf->Output();

exit;
?>

