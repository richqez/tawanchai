<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';

if (isset($_POST['submit'])) {
    echo '<meta charset="utf-8">';
    $payment_id = NextPayment_id();
    if ($_POST['payment_type'] == 'cash') {
        $sql = 'INSERT INTO payment '
                . '(payment_id, payment_date, invoice_id, payment_type, payment_money, payment_remark) '
                . 'VALUES '
                . '("' . $payment_id . '", NOW(),"' . $_GET['id'] . '", "' . $_POST['payment_type'] . '", "' . $_POST['payment_money'] . '", "' . $_POST['payment_remark'] . '") ';
    } else {
        if ($_POST['payment_bankname'] == '' || trim($_POST['payment_cheque']) == '' || $_POST['payment_chequedate'] == '') {
            echo '<script>alert("ใส่รายละเอียดเช็ค !!!");window.history.back();</script>';
            exit();
        }

        $sql = 'INSERT INTO payment '
                . '(payment_id, payment_date, invoice_id, payment_type, payment_bankname, payment_cheque, payment_chequedate, payment_money, payment_remark) '
                . 'VALUES '
                . '("' . $payment_id . '", NOW(),"' . $_GET['id'] . '", "' . $_POST['payment_type'] . '", "' . $_POST['payment_bankname'] . '", "' . $_POST['payment_cheque'] . '", "' . $_POST['payment_chequedate'] . '", "' . $_POST['payment_money'] . '", "' . $_POST['payment_remark'] . '") ';
    }
    $result = mysql_query($sql);
    if ($result) {
        $sql = 'UPDATE invoice SET invoice_status = "1" WHERE invoice_id = "' . $_GET['id'] . '"';
        mysql_query($sql);

        echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!");window.open("payment_print.php?id=' . $payment_id . '","_blank");window.focus();</script>';
        echo '<meta http-equiv="refresh" content="1; URL = payment.php"/>';
        exit();
    } else {
        echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ !!!");window.history.back();</script>';
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="lib/pagination/style.css" rel="stylesheet" type="text/css"/>        
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <style>
            .table > tbody > tr > td {
                vertical-align: top;
            }

        </style>
    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="invoice.php">ใบแจ้งหนี้</a></li>
                    <li class="active">ชำระเงิน</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ใบแจ้งหนี้ (ชำระเงิน)</h1>
                </div>
            </div><!--/.row-->
            <?php
            $sql = 'SELECT * FROM customer, invoice '
                    . 'WHERE '
                    . 'customer.cus_id = invoice.cus_id AND '
                    . 'invoice.invoice_id = "' . $_GET['id'] . '" ';
            $result = mysql_query($sql);
            $row = mysql_fetch_array($result);
            $iv_status = $row['invoice_status'];
            ?>
            <form name="paymentform" id="quotationform" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><span class="glyphicon glyphicon-usd"></span> รายละเอียดการชำระเงิน</div>
                            <div class="panel-body">  
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="form-group">
                                        <label>ประเภทการชำะเงิน</label>
                                    </div>
                                    <div class="form-group">
                                        <input name="payment_type" type="radio" id="payment_type1" placeholder="เงินสด *" value="cash"> เงินสด 
                                    </div>
                                    <div class="form-group">
                                        <input name="payment_type" type="radio" id="payment_type2" placeholder="เงินสด *" value="cheque" checked> เช็คธนาคาร
                                    </div>
                                    <div id="bankinfo">
                                        <div class="form-group">
                                            <label>เช็คธนาคาร</label>
                                            <select class="form-control" name="payment_bankname" id="payment_bankname">
                                                <?php BankName(); ?>
                                            </select>  
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>เลขที่เช็ค</label>
                                                    <input class="form-control" name="payment_cheque" type="number" id="payment_cheque" placeholder="เลขที่เช็ค *" maxlength="8"> 
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ลงวันที่</label>
                                                    <input class="form-control" name="payment_chequedate" type="date" id="payment_chequedate" placeholder="เช็คลงวันที่ *"> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>จำนวนเงิน</label>
                                                <input class="form-control" name="" type="text" placeholder="จำนวนเงิน *" value="<?php echo number_format($row['invoice_total'], 2); ?>" readonly> 
                                                <input name="payment_money" type="hidden" id="payment_money" placeholder="จำนวนเงิน *" value="<?php echo $row['invoice_total']; ?>"> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>หมายเหตุ</label>
                                        <textarea class="form-control" name="payment_remark" id="payment_remark" placeholder="หมายเหตุ *" rows="5"></textarea>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> บันทึก</button>		
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.col-->
                </div>
            </form>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">                        
                        <div class="panel-body"> 

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <?php echo COMPANY_NAME; ?>
                                    <h1 class="page-header">ใบแจ้งหนี้</h1>
                                    <h2>ค่าขนส่งประจำเดือน <?php echo $month_short[$row['invoice_month']] . ' ' . ($row['invoice_year'] + 543); ?></h2>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <p><strong>รหัสลูกค้า : </strong><?php echo $row['cus_id']; ?></p>
                                    <p><strong>ชื่อบริษัท : </strong><?php echo $row['cus_name']; ?></p>
                                    <p><strong>ที่อยู่ : </strong><?php echo $row['cus_address']; ?></p>
                                    <p><strong>เบอร์โทร : </strong><?php echo $row['cus_tel']; ?></p>
                                    <p><strong>E-mail : </strong><?php echo $row['cus_email']; ?> <strong>Line ID : </strong><?php echo $row['cus_line']; ?></p>
                                    <p><strong>ผู้ประสานงาน : </strong><?php echo $row['cus_contact']; ?></p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <p><strong>เลขที่ : </strong><?php echo $_GET['id']; ?></p>
                                    <p><strong>วันที่ : </strong><?php echo ThaidateNoTime($row['invoice_date']); ?></p>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="80" class="text-center">ลำดับที่</th>
                                            <th width="100" class="text-center">วันที่</th>
                                            <th class="text-center">รายละเอียด</th>
                                            <th width="100" class="text-center">จำนวนเที่ยว</th>
                                            <th width="150" class="text-center">ราคาต่อเที่ยว</th>
                                            <th width="200" class="text-center">ราคารวม</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = 'SELECT * FROM quotation, invoice, invoicedetail '
                                                . 'WHERE '
                                                . 'invoice.invoice_id = invoicedetail.invoice_id AND '
                                                . 'invoicedetail.quo_id = quotation.quo_id AND '
                                                . 'invoice.invoice_id = "' . $_GET['id'] . '" '
                                                . 'ORDER BY quotation.quo_date ASC ';
                                        $result = mysql_query($sql);
                                        $i = 1;
                                        $quo_total = 0;
                                        while ($row = mysql_fetch_array($result)) {
                                            $quo_total += $row['quo_total'];
                                            ?>
                                            <tr>
                                                <td class="text-center" style="vertical-align: top;"><?php echo $i; ?></td>
                                                <td class="text-center" style="vertical-align: top;"><?php echo ThaidateNoTime($row['quo_date']); ?></td>
                                                <td>
                                                    <?php
                                                    echo '<p><strong>ข้อมูลปลายทาง</strong></p>';
                                                    echo 'ชื่อ-นามสกุล : ' . $row['quo_name'] . ' <br>';
                                                    echo 'ที่อยู่ : ' . $row['quo_address'] . ' <br>';
                                                    echo 'เบอร์โทร : ' . $row['quo_tel'] . ' <br>';
                                                    echo 'E-mail : ' . $row['quo_mail'] . ' Line ID : ' . $row['quo_line'] . '<br><br>';

                                                    echo '<p><strong>รายละเอียดสินค้า</strong></p>';
                                                    echo 'ขนส่ง ' . $row['quo_product'] . ' ';
                                                    echo 'จำนวน ' . $row['quo_count'] . ' ' . $row['quo_unit'] . ' ';
                                                    echo '<br>';
                                                    echo 'ต้นทาง ' . Province_name($row['startpoint']) . ' ถึงปลายทาง ' . Province_name($row['endpoint']) . ' ';
                                                    echo '<br>';
                                                    echo 'รวมระยะทาง ' . $row['quo_distance'] . ' กิโลเมตร';
                                                    echo '<br>';
                                                    echo 'ตั้งแต่วันที่ ' . ThaidateNoTime($row['quo_startdate']) . ' ถึงวันที่ ' . ThaidateNoTime($row['quo_enddate']) . ' ';
                                                    echo '<br>';
                                                    echo '<br>';
                                                    echo 'ใช้ ' . GetTrucktype($row['trucktype_id']) . ' ทะเบียนรถ ' . GetTruck($row['truck_id']);
                                                    echo '<br>';
                                                    echo 'พนักงานขับรถ ' . Getdriver2($row['drv_id']);
                                                    ?>
                                                </td>
                                                <td class="text-center" style="vertical-align: top;"><?php echo $row['quo_number']; ?></td>
                                                <td class="text-center" style="vertical-align: top;">
                                                    <?php
                                                    echo number_format($row['quo_price'], 2);
                                                    ?>
                                                </td>
                                                <td class="text-right" style="vertical-align: top;">
                                                    <?php
                                                    echo number_format($row['quo_total'], 2);
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5" class="text-right">
                                                <strong>รวมทั้งสิ้น</strong>
                                                <input type="hidden" name="invoice_total" value="<?php echo $quo_total ?>">
                                            </td>
                                            <td class="text-right"><strong><?php echo number_format($quo_total, 2); ?></strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.col-->
                </div>
            </div>
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-table.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript">
            !function ($) {
                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function () {
                if ($(window).width() > 768)
                    $('#sidebar-collapse').collapse('show');
            });
            $(window).on('resize', function () {
                if ($(window).width() <= 767)
                    $('#sidebar-collapse').collapse('hide');
            });

            $(function () {
                $('#payment_type1').attr('checked');
                $('#payment_type1').change(function () {
                    $('#bankinfo').hide();
                });
                $('#payment_type2').change(function () {
                    $('#bankinfo').show();
                });
            });
        </script>
    </body>
</html>
