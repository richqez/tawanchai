<?php
include 'lib/config.php';
include 'lib/conn.php';
include 'lib/function.php';
?>
<script type="text/javascript">
    $(function () {
        $('.usr').click(function () {
            $.post('invoice_detail.php', {id: $(this).data('id')},
            function (data) {
                $('#cusdata').html(data);
            });
        });
    });
</script>

<?php
if ($_POST['page']) {
    $page = $_POST['page'];
    $cur_page = $page;
    $page -= 1;
    $per_page = 20;
    $previous_btn = TRUE;
    $next_btn = TRUE;
    $first_btn = TRUE;
    $last_btn = TRUE;
    $start = $page * $per_page;

    $opt = '';

    if ($_POST['invoice_status'] != '') {
        $opt = ' AND invoice.invoice_status = "' . $_POST['invoice_status'] . '" ';
    }
    ?>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th class="text-center">ลำดับที่</th>
                    <th class="text-center">เลขที่ใบแจ้งหนี้</th>
                    <th class="text-center">วันที่</th>
                    <th class="text-center">กำหนดชำระเงิน</th>
                    <th class="text-center">บริษัท</th>
                    <th class="text-center">จำนวนเงิน</th>
                    <th class="text-center">สถานะ</th>
                    <th class="text-center">รายละเอียด</th>
                    <th class="text-center">พิมพ์</th>
                    <th class="text-center">ชำระเงิน</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = 'SELECT * FROM customer, invoice '
                        . 'WHERE '
                        . 'customer.cus_id = invoice.cus_id ' . $opt
                        . 'ORDER BY invoice.invoice_id DESC '
                        . 'LIMIT ' . $start . ',' . $per_page;
                $result = mysql_query($sql);
                if (mysql_num_rows($result) == 0) {
                    echo '<tr><td colspan="9" class="text-danger" align="center">ไม่พบข้อมูล</td></tr>';
                } else {
                    $i = 1;
                    while ($row = mysql_fetch_array($result)) {
                        if ($row['invoice_status'] == 0) {
                            $bg = 'info';
                        } else {
                            $bg = '';
                        }
                        ?>
                        <tr class="<?php echo $bg; ?>">
                            <td class="text-center"><?php echo $i + $start; ?></td>
                            <td class="text-center"><?php echo $row['invoice_id']; ?></td>
                            <td class="text-center"><?php echo ThaidatenoTime($row['invoice_date']); ?></td>
                            <td class="text-center"><?php echo ThaidatenoTime($row['invoice_due']); ?></td>
                            <td><?php echo $row['cus_name']; ?></td>
                            <td class="text-center"><?php echo number_format($row['invoice_total'], 2); ?></td>
                            <td class="text-center"><?php echo $invoice_status[$row['invoice_status']]; ?></td>
                            <td class="text-center">
                                <a class="btn btn-info btn-sm usr" title="รายละเอียด" data-toggle="modal" data-target="#myModal" data-id="<?php echo $row['invoice_id']; ?>">
                                    <span class="glyphicon glyphicon-th-large"></span>
                                </a> 
                            </td>
                            <td class="text-center">
                                <a class="btn btn-warning btn-sm" href="invoice_print.php?id=<?php echo $row['invoice_id']; ?>" title="พิมพ์" target="_blank"> <span class="glyphicon glyphicon-print"></span></a>
                            </td>
                            <td class="text-center">
                                <?php
                                if ($row['invoice_status'] == 0) {
                                    ?>
                                    <a class="btn btn-primary btn-sm" href="invoice_payment.php?id=<?php echo $row['invoice_id']; ?>" title="พิมพ์"> <span class="glyphicon glyphicon-arrow-right"></span></a>
                                    <?php
                                } else {
                                    $sql = 'SELECT * FROM payment WHERE invoice_id = "' . $row['invoice_id'] . '"';
                                    $rs = mysql_query($sql);
                                    $r = mysql_fetch_array($rs);
                                    ?>

                                    <a class="btn btn-primary btn-sm" href="payment_print.php?id=<?php echo $r['payment_id'] ?>" title="พิมพ์" target="_blank"> <span class="glyphicon glyphicon-check"></span></a>
                                        <?php
                                    }
                                    ?>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
            </tbody>
        </table>
    </div>

    <?php
    $query_pag_num = 'SELECT COUNT(*) AS count FROM customer, invoice '
            . 'WHERE '
            . 'customer.cus_id = invoice.cus_id ' . $opt;
    $result_pag_num = mysql_query($query_pag_num);
    $row = mysql_fetch_array($result_pag_num);
    $count = $row['count'];
    $no_of_paginations = ceil($count / $per_page);

    include 'lib/pagination/pagination.php';
}