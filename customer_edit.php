<?php
session_start();
include 'lib/function.php';
Checklogin();
include 'lib/conn.php';
include 'lib/config.php';

if (isset($_POST['submit'])) {
    echo '<meta charset="utf-8">';
    $sql = 'SELECT * FROM customer WHERE cus_email = "' . $_POST['cus_email'] . '" AND cus_id != "' . $_GET['id'] . '"';
    $recheackmail = mysql_query($sql);
    if (mysql_num_rows($recheackmail) > 0) {
        echo '<script>alert("อีเมล์นี้ มีผู้ใช้งานแล้ว !!!");window.history.back();</script>';
        exit();
    }

    $sql = 'UPDATE customer SET '
            . 'cus_name = "' . trim($_POST['cus_name']) . '", '
            . 'cus_address = "' . trim($_POST['cus_address']) . '", '
            . 'cus_tel = "' . trim($_POST['cus_tel']) . '", '
            . 'cus_email = "' . trim($_POST['cus_email']) . '", '
            . 'cus_contact = "' . trim($_POST['cus_contact']) . '", ' 
            . 'cus_about = "' . trim($_POST['cus_about']) . '", ' 
            . 'cus_line = "' . trim($_POST['cus_line']) . '" ' 
            . 'WHERE '
            . 'cus_id = "' . $_GET['id'] . '" ';
    $result = mysql_query($sql);
    if ($result) {
        echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!")</script>';
        echo '<meta http-equiv="refresh" content="1; URL = customer.php"/>';
        exit();
    } else {
        echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ !!!");window.history.back();</script>';
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="../lib/pagination/style.css" rel="stylesheet" type="text/css"/>
        <link href="../mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="customer.php">ลูกค้า</a></li>
                    <li class="active">แก้ไขลูกค้า</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">ลูกค้า</h2>
                </div>
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-pencil"></span> แก้ไขลูกค้า</div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3">
                                <?php
                                $sql = 'SELECT * FROM customer '
                                        . 'WHERE '
                                        . 'cus_id = "' . $_GET['id'] . '"';
                                $result = mysql_query($sql);
                                $row = mysql_fetch_array($result);
                                ?>
                                <form name="customerform" id="customerform" action="" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>ชื่อบริษัท</label>
                                        <input class="form-control" name="cus_name" type="text" id="cus_name" placeholder="ชื่อ-นามสกุล *" value="<?php echo $row['cus_name'] ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <label>ประกอบธุรกิจเกี่ยวกับ</label>
                                        <input class="form-control" name="cus_about" type="text" id="cus_about" placeholder="ประกอบธุรกิจเกี่ยวกับ *" value="<?php echo $row['cus_about'] ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <label>ที่อยู่</label>
                                        <textarea class="form-control" name="cus_address" id="cus_address" placeholder="ที่อยู่ *"><?php echo $row['cus_address'] ?></textarea>    
                                    </div>
                                    <div class="form-group">
                                        <label>เบอร์โทร</label>
                                        <input class="form-control" name="cus_tel" type="text" id="cus_tel" placeholder="เบอร์โทรศัพท์ *" value="<?php echo $row['cus_tel'] ?>" maxlength="10"> 
                                    </div>
                                    <div class="form-group">
                                        <label>อีเมล์</label>
                                        <input class="form-control" name="cus_email" type="email" id="cus_email" placeholder="อีเมล์ *" value="<?php echo $row['cus_email'] ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <label>Line ID</label>
                                        <input class="form-control" name="cus_line" type="text" id="cus_line" placeholder="Line ID *" value="<?php echo $row['cus_line'] ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <label>ผู้ประสานงาน</label>
                                        <input class="form-control" name="cus_contact" type="text" id="cus_contact" placeholder="ผู้ประสานงาน *" value="<?php echo $row['cus_contact'] ?>"> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> บันทึก</button>		
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div><!-- /.row -->
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="../js/jquery.validate.js" type="text/javascript"></script>
        <script src="../js/additional-methods.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('#customerform').validate({
                    rules: {
                        cus_name: {
                            required: true
                        },
                        cus_about: {
                            required: true
                        },
                        cus_address: {
                            required: true
                        },
                        cus_tel: {
                            required: true,
                            number: true,
                            minlength: 10
                        },
                        cus_email: {
                            required: true,
                            email: true
                        },
                        cus_type: {
                            required: true
                        },
                        cus_contact: {
                            required: true
                        }
                    },
                    messages: {
                        cus_name: {
                            required: 'กรอกชื่อ-นามสกุล'
                        },
                        cus_about: {
                            required: 'กรอกข้อมูลประกอบธุรกิจเกี่ยวกับ'
                        },
                        cus_address: {
                            required: 'กรอกที่อยู่'
                        },
                        cus_tel: {
                            required: 'กรอกเบอร์โทรศัพท์',
                            number: 'เป็นตัวเลขเท่านั้น',
                            minlength: 'ต้องเป็น 10 หลัก'
                        },
                        cus_email: {
                            required: 'กรอกอีเมล์',
                            email: 'รูปแบบอีเมล์ไม่ถูกต้อง'
                        },
                        cus_type: {
                            required: 'เลือกประเภทลูกค้า'
                        },
                        cus_contact: {
                            required: 'กรอกผู้ประสานงาน'
                        }
                    }
                });
            });
        </script>
    </body>
</html>
