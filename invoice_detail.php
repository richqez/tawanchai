<?php
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/function.php';

$sql = 'SELECT * FROM customer, invoice '
        . 'WHERE '
        . 'customer.cus_id = invoice.cus_id AND '
        . 'invoice.invoice_id = "' . $_POST['id'] . '" ';
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
$iv_status = $row['invoice_status'];
?>
<div class="row">
    <div class="col-md-12 text-center">
        <?php echo COMPANY_NAME; ?>

        <h2>ใบแจ้งหนี้</h2>
        <h2>ค่าขนส่งประจำเดือน <?php echo $month_short[$row['invoice_month']] . ' ' . ($row['invoice_year'] + 543); ?></h2>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <p><strong>รหัสลูกค้า : </strong><?php echo $row['cus_id']; ?></p>
        <p><strong>ชื่อบริษัท : </strong><?php echo $row['cus_name']; ?></p>
        <p><strong>ที่อยู่ : </strong><?php echo $row['cus_address']; ?></p>
        <p><strong>เบอร์โทร : </strong><?php echo $row['cus_tel']; ?> <strong>อีเมล์ : </strong><?php echo $row['cus_email']; ?></p>
        <p><strong>ผู้ประสานงาน : </strong><?php echo $row['cus_contact']; ?></p>
    </div>
    <div class="col-md-6 text-right">
        <p><strong>เลขที่ : </strong><?php echo $_POST['id']; ?></p>
        <p><strong>วันที่ : </strong><?php echo ThaidateNoTime($row['invoice_date']); ?></p>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th width="80" class="text-center">ลำดับที่</th>
                <th width="100" class="text-center">วันที่</th>
                <th class="text-center">รายละเอียด</th>
                <th width="100" class="text-center">จำนวนเที่ยว</th>
                <th width="100" class="text-center">ราคาต่อเที่ยว</th>
                <th width="150" class="text-center">ราคารวม</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sql = 'SELECT * FROM quotation, invoice, invoicedetail '
                    . 'WHERE '
                    . 'invoice.invoice_id = invoicedetail.invoice_id AND '
                    . 'invoicedetail.quo_id = quotation.quo_id AND '
                    . 'invoice.invoice_id = "'.$_POST['id'].'" '
                    . 'ORDER BY quotation.quo_date ASC ';
            $result = mysql_query($sql);
            $i = 1;
            $quo_total = 0;
            while ($row = mysql_fetch_array($result)) {
                $quo_total += $row['quo_total'];
                ?>
                <tr>
                    <td class="text-center"><?php echo $i; ?></td>
                    <td class="text-center"><?php echo ThaidateNoTime($row['quo_date']); ?></td>
                    <td>
                        <?php
                        echo 'ขนส่ง ' . $row['quo_product'] . ' ';
                        echo 'จำนวน ' . $row['quo_count'] . ' ' . $row['quo_unit'] . ' ';
                        echo '<br>';
                        echo 'ต้นทาง ' . Province_name($row['startpoint']) . ' ถึงปลายทาง ' . Province_name($row['endpoint']) . ' ';
                        echo '<br>';
                        echo 'รวมระยะทาง ' . $row['quo_distance'] . ' กิโลเมตร';
                        echo '<br>';
                        echo 'ตั้งแต่วันที่ ' . ThaidateNoTime($row['quo_startdate']) . ' ถึงวันที่ ' . ThaidateNoTime($row['quo_enddate']) . ' ';
                        echo '<br>';
                        echo '<br>';
                        echo 'ใช้ ' . GetTrucktype($row['trucktype_id']) . ' ทะเบียนรถ ' . GetTruck($row['truck_id']);
                        echo '<br>';
                        echo 'พนักงานขับรถ ' . Getdriver2($row['drv_id']);
                        ?>
                    </td>
                    <td class="text-center"><?php echo $row['quo_number']; ?></td>
                    <td class="text-center">
                        <?php
                        echo number_format($row['quo_price'], 2);
                        ?>
                    </td>
                    <td class="text-right">
                        <?php
                        echo number_format($row['quo_total'], 2);
                        ?>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5" class="text-right">
                    <strong>รวมทั้งสิ้น</strong>
                </td>
                <td class="text-right"><strong><?php echo number_format($quo_total, 2); ?></strong></td>
            </tr>
        </tfoot>
    </table>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <h4>สถานะ : <?php echo $invoice_status[$iv_status]; ?></h4>
    </div>
</div>

<?php
if ($row['quo_status'] == '0') {
    ?>

    <div class="row">
        <hr>
        <div class="col-md-12 text-center">
            <a class="btn btn-primary" href="quotation_status.php?id=<?php echo $row['quo_id'] ?>&status=1" title="ยกเลิก" onclick="return confirm('อนุมัติใบเสนอราคานี้ใช่หรือไม่ ?');"><span class="glyphicon glyphicon-check"></span> ยืนยัน</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-danger" href="quotation_status.php?id=<?php echo $row['quo_id'] ?>&status=2" title="ยกเลิก" onclick="return confirm('ยกเลิกใบเสนอราคานี้ใช่หรือไม่ ?');"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</a>
        </div>
    </div>
    <?php
}