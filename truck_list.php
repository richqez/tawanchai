<?php
include 'lib/config.php';
include 'lib/conn.php';
include 'lib/function.php';
?>
<script type="text/javascript">
    $(function () {
        $('.truck').click(function () {
            $.post('truck_detail.php', {id: $(this).data('id')},
            function (data) {
                $('#truckdata').html(data);
            });
        });
    });
</script>

<?php
if ($_POST['page']) {
    $page = $_POST['page'];
    $cur_page = $page;
    $page -= 1;
    $per_page = 20;
    $previous_btn = TRUE;
    $next_btn = TRUE;
    $first_btn = TRUE;
    $last_btn = TRUE;
    $start = $page * $per_page;

    $opt = '';

    if ($_POST['search-text'] != '') {
        $opt .= ' AND (truck.truck_license LIKE "%' . $_POST['search-text'] . '%" OR driver.drv_name LIKE "%'.$_POST['search-text'].'%")';
        echo '<p align="center"><strong>ผลการค้นหา "' . $_POST['search-text'] . '"<br>';
        echo '<a href="">แสดงทั้งหมด</a></strong></p>';
    }

    if ($_POST['trucktype_id'] != '') {
        $opt = ' AND truck.trucktype_id = "' . $_POST['trucktype_id'] . '" ';
    }

    if ($_POST['fuel_id'] != '') {
        $opt = ' AND truck.fuel_id = "' . $_POST['fuel_id'] . '" ';
    }
    ?>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="text-center">รหัสรถบรรทุก</th>
                    <th class="text-center">ประเภทรถบรรทุก</th>
                    <th class="text-center">ทะเบียนรถ</th>
                    <th class="text-center">เชื้อเพลิง</th>
                    <th class="text-center">พนักงานขับรถ</th>
                    <th class="text-center">ค่าเช่า/เที่ยว</th>
                    <th width="100" class="text-center">รายละเอียด</th>
                    <th width="80" class="text-center">แก้ไข</th>
                    <th width="80" class="text-center">สถานะ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = 'SELECT '
                        . 'truck.truck_id,'
                        . 'truck.truck_license, '
                        . 'province.province_name, '
                        . 'driver.drv_name, '
                        . 'trucktype.trucktype_name,'
                        . 'fuel.fuel_name,'
                        . 'truck.truck_drv,'
                        . 'truck.truck_rent, '
                        . 'truck.truck_status '
                        . 'FROM '
                        . 'truck LEFT JOIN driver ON driver.truck_id = truck.truck_id, trucktype, fuel, province '
                        . 'WHERE '
                        . 'truck.fuel_id = fuel.fuel_id AND '
                        . 'trucktype.trucktype_id = truck.trucktype_id AND '
                        . 'province.province_id = truck.province_id ' . $opt
                        . 'ORDER BY truck.truck_id DESC '
                        . 'LIMIT ' . $start . ',' . $per_page;                
                $result = mysql_query($sql);
                if (mysql_num_rows($result) == 0) {
                    echo '<tr><td colspan="8" class="text-danger" align="center">ไม่พบข้อมูล</td></tr>';
                } else {
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $row['truck_id'] ?></td>
                            <td class="text-center"><?php echo $row['trucktype_name'] ?></td>
                            <td class="text-center"><?php echo $row['truck_license'] . '<br>' . $row['province_name']; ?></td>
                            <td class="text-center"><?php echo $row['fuel_name']; ?></td>
                            <td class="text-center">
                                <?php
                                if ($row['truck_drv'] == '1') {
                                    $drivername = Getdriver($row['truck_id']);
                                    echo $drivername;
                                } else {
                                    echo '<span class="text-danger">==ยังไม่มีพนักงานขับรถ==</span>';
                                }
                                ?>
                            </td>
                            <td class="text-center"><?php echo number_format($row['truck_rent'], 2); ?></td>
                            <td class="text-center">
                                <a class="btn btn-info btn-sm truck" title="รายละเอียด" data-toggle="modal" data-target="#myModal" data-id="<?php echo $row['truck_id']; ?>">
                                    <span class="glyphicon glyphicon-th-large"></span>
                                </a> 
                            </td>
                            <td class="text-center">
                                <a class="btn btn-warning btn-sm" href="truck_edit.php?id=<?php echo $row['truck_id']; ?>" title="แก้ไข"> <span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                            <td class="text-center">
                                <a href="truck_status.php?id=<?php echo $row['truck_id']; ?>&status=<?php echo $row['truck_status']; ?>" title="เปลี่ยนสถานะ"  onclick="return confirm('ต้องการเปลี่ยนสถานะ ?');"><?php ShowTruckstatus($row['truck_status']); ?></a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>

    <?php
    $query_pag_num = 'SELECT COUNT(*) AS count FROM truck LEFT JOIN driver ON driver.truck_id = truck.truck_id , trucktype, fuel, province '
            . ' WHERE '
            . 'truck.fuel_id = fuel.fuel_id AND '
            . 'trucktype.trucktype_id = truck.trucktype_id AND '
            . 'province.province_id = truck.province_id ' . $opt;
    $result_pag_num = mysql_query($query_pag_num);
    $row = mysql_fetch_array($result_pag_num);
    $count = $row['count'];
    $no_of_paginations = ceil($count / $per_page);

    include 'lib/pagination/pagination.php';
} else {
    echo '<meta http-equiv="refresh" content="1; URL = truck.php"/>';
    exit();
}