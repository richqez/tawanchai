<?php
include 'lib/config.php';
include 'lib/conn.php';
include 'lib/function.php';
?>
<script type="text/javascript">
    $(function () {
        $('.usr').click(function () {
            $.post('customer_detail.php', {id: $(this).data('id')},
            function (data) {
                $('#cusdata').html(data);
            });
        });
    });
</script>

<?php
if ($_POST['page']) {
    if ($_POST['invoice_month'] != '' || $_POST['invoice_year'] != '') {
        $page = $_POST['page'];
        $cur_page = $page;
        $page -= 1;
        $per_page = 20;
        $previous_btn = TRUE;
        $next_btn = TRUE;
        $first_btn = TRUE;
        $last_btn = TRUE;
        $start = $page * $per_page;

        $invoice_month = $_POST['invoice_month'];
        $invoice_year = $_POST['invoice_year'];
        ?>
        <div class="row">
            <div class="col-md-12 text-center">
                <h4>ค่าขนส่งประจำเดือน <?php echo $month_short[$_POST['invoice_month']] . ' ' . ($_POST['invoice_year'] + 543); ?></h4>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="100" class="text-center">รหัสลูกค้า</th>
                        <th class="text-center">ชื่อบริษัท</th>
                        <th width="200"class="text-center">จำนวนรายการ</th>
                        <th width="220"class="text-center">จำนวนเงิน</th>
                        <th width="200" class="text-center">ใบแจ้งหนี้</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql = 'SELECT customer.cus_id, customer.cus_name, '
                            . 'Count(quotation.quo_id) AS count_quo, Sum(quotation.quo_total) AS sum_quo '
                            . 'FROM customer, quotation '
                            . 'WHERE '
                            . 'quotation.cus_id = customer.cus_id AND '
                            . 'quotation.quo_status = "1" AND '
                            . 'quotation.quo_invoice = "0" AND '
                            . 'MONTH(quotation.quo_date) = ' . $_POST['invoice_month'] . ' AND '
                            . 'YEAR(quotation.quo_date) = ' . $_POST['invoice_year'] . ' '
                            . 'GROUP BY '
                            . 'customer.cus_id, customer.cus_name '
                            . 'ORDER BY customer.cus_id ASC '
                            . 'LIMIT ' . $start . ',' . $per_page;

                    $result = mysql_query($sql);
                    if (mysql_num_rows($result) == 0) {
                        echo '<tr><td colspan="6" class="text-danger" align="center">ไม่พบข้อมูล</td></tr>';
                    } else {
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $row['cus_id'] ?></td>
                                <td><?php echo $row['cus_name']; ?></td>
                                <td class="text-center"><?php echo number_format($row['count_quo']); ?></td>
                                <td class="text-center"><?php echo number_format($row['sum_quo'], 2); ?></td>
                                <td class="text-center">
                                    <a class="btn btn-warning btn-sm" href="invoice_add2.php?id=<?php echo $row['cus_id']; ?>&m=<?php echo $_POST['invoice_month']; ?>&y=<?php echo $_POST['invoice_year']; ?>" title="สร้างใบแจ้งหนี้"> <span class="glyphicon glyphicon-arrow-right"></span></a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>

        <?php
        $query_pag_num = 'SELECT COUNT(*) AS count '
                . 'FROM customer, quotation '
                . 'WHERE '
                . 'quotation.cus_id = customer.cus_id AND '
                . 'quotation.quo_status = "1" AND '
                . 'quotation.quo_invoice = "0" AND '
                . 'MONTH(quotation.quo_date) = ' . $_POST['invoice_month'] . ' AND '
                . 'YEAR(quotation.quo_date) = ' . $_POST['invoice_year'] . ' '
                . 'GROUP BY '
                . 'customer.cus_id, customer.cus_name ';
        $result_pag_num = mysql_query($query_pag_num);
        $row = mysql_fetch_array($result_pag_num);
        $count = $row['count'];
        $no_of_paginations = ceil($count / $per_page);

        include 'lib/pagination/pagination.php';
    }
}