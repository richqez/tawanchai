<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/config.php';
include 'lib/conn.php';

if ($_SESSION['emp_type'] == '3') {
    header('location: employee.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">

        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li class="active">หน้าหลัก</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">หน้าหลัก</h1>
                </div>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">ใบเสนอราคาใหม่</div>
                        <div class="panel-body">                            
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th width="100" class="text-center">ลำดับที่</th>
                                            <th class="text-center">เลขที่ใบเสนอราคา</th>
                                            <th class="text-center">วันที่</th>
                                            <th class="text-center">บริษัท</th>
                                            <th width="100" class="text-center">จำนวนเงิน</th>
                                            <th width="100" class="text-center">สถานะ</th>
                                            <th width="100" class="text-center">รายละเอียด</th>
                                            <th width="100" class="text-center">พิมพ์</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = 'SELECT * FROM customer, quotation '
                                                . 'WHERE '
                                                . 'customer.cus_id = quotation.cus_id  AND quotation.quo_status = "0" '
                                                . 'ORDER BY quotation.quo_id DESC '
                                                . 'LIMIT 0,5';
                                        $result = mysql_query($sql);
                                        if (mysql_num_rows($result) == 0) {
                                            echo '<tr><td colspan="8" class="text-danger" align="center">ไม่พบข้อมูล</td></tr>';
                                        } else {
                                            $i = 1;
                                            while ($row = mysql_fetch_array($result)) {
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $i; ?></td>
                                                    <td class="text-center"><?php echo $row['quo_id']; ?></td>
                                                    <td class="text-center"><?php echo ThaidatenoTime($row['quo_date']); ?></td>
                                                    <td><?php echo $row['cus_name']; ?></td>
                                                    <td class="text-center"><?php echo number_format($row['quo_total'], 2); ?></td>
                                                    <td class="text-center"><?php echo $quo_status[$row['quo_status']]; ?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-info btn-sm q" title="รายละเอียด" data-toggle="modal" data-target="#myModal_1" data-id="<?php echo $row['quo_id']; ?>">
                                                            <span class="glyphicon glyphicon-th-large"></span>
                                                        </a> 
                                                    </td>
                                                    <td class="text-center">
                                                        <a class="btn btn-warning btn-sm" href="quotation_print.php?id=<?php echo $row['quo_id']; ?>" title="พิมพ์" target="_blank"> <span class="glyphicon glyphicon-print"></span></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="quotation.php">แสดงทั้งหมด</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div>


            <p>&nbsp;</p>


            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">ใบแจ้งหนี้ใหม่</div>
                        <div class="panel-body">                            
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th width="100" class="text-center">ลำดับที่</th>
                                            <th class="text-center">เลขที่ใบแจ้งหนี้</th>
                                            <th class="text-center">วันที่</th>
                                            <th class="text-center">บริษัท</th>
                                            <th width="100" class="text-center">จำนวนเงิน</th>
                                            <th width="100" class="text-center">สถานะ</th>
                                            <th width="100" class="text-center">รายละเอียด</th>
                                            <th width="100" class="text-center">พิมพ์</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = 'SELECT * FROM customer, invoice '
                                                . 'WHERE '
                                                . 'customer.cus_id = invoice.cus_id  AND invoice.invoice_status = "0" '
                                                . 'ORDER BY invoice.invoice_id DESC '
                                                . 'LIMIT 0,5';
                                        $result = mysql_query($sql);
                                        if (mysql_num_rows($result) == 0) {
                                            echo '<tr><td colspan="8" class="text-danger" align="center">ไม่พบข้อมูล</td></tr>';
                                        } else {
                                            $i = 1;
                                            while ($row = mysql_fetch_array($result)) {
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $i; ?></td>
                                                    <td class="text-center"><?php echo $row['invoice_id']; ?></td>
                                                    <td class="text-center"><?php echo ThaidatenoTime($row['invoice_date']); ?></td>
                                                    <td><?php echo $row['cus_name']; ?></td>
                                                    <td class="text-center"><?php echo number_format($row['invoice_total'], 2); ?></td>
                                                    <td class="text-center"><?php echo $invoice_status[$row['invoice_status']]; ?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-info btn-sm i" title="รายละเอียด" data-toggle="modal" data-target="#myModal_2" data-id="<?php echo $row['invoice_id']; ?>">
                                                            <span class="glyphicon glyphicon-th-large"></span>
                                                        </a> 
                                                    </td>
                                                    <td class="text-center">
                                                        <a class="btn btn-warning btn-sm" href="invoice_print.php?id=<?php echo $row['invoice_id']; ?>" title="พิมพ์" target="_blank"> <span class="glyphicon glyphicon-print"></span></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="invoice.php">แสดงทั้งหมด</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div>



            <div class="modal fade" id="myModal_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">รายละเอียดใบเสนอราคา</h4>
                        </div>
                        <div class="modal-body">                            
                            <div id="qdata"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="myModal_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">รายละเอียดใบแจ้งหนี้</h4>
                        </div>
                        <div class="modal-body">                            
                            <div id="idata"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script>
            $('#calendar').datepicker({
            });

            !function ($) {
                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function () {
                if ($(window).width() > 768)
                    $('#sidebar-collapse').collapse('show')
            })
            $(window).on('resize', function () {
                if ($(window).width() <= 767)
                    $('#sidebar-collapse').collapse('hide')
            })
        </script>
        <script type="text/javascript">
            $(function () {
                $('.q').click(function () {
                    $.post('quotation_detail.php', {id: $(this).data('id')},
                    function (data) {
                        $('#qdata').html(data);
                    });
                });
            });
        </script>

        <script type="text/javascript">
            $(function () {
                $('.i').click(function () {
                    $.post('invoice_detail.php', {id: $(this).data('id')},
                    function (data) {
                        $('#idata').html(data);
                    });
                });
            });
        </script>
    </body>

</html>
