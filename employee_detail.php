<?php
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/function.php';
?>
<div class="row">
    <?php
    $sql = 'SELECT * FROM employee '
            . 'WHERE '
            . 'emp_id = "' . $_POST['id'] . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    ?>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"></div>
        <div class="col-xs-6">
            <p>
                <img src="images/employee/<?php echo $row['emp_image']; ?>" alt="<?php echo $row['emp_id']; ?>" title="<?php echo $row['emp_id']; ?>" class="img-thumbnail">
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>รหัสพนักงาน :</strong></p></div>
        <div class="col-xs-6"><p><?php echo $row['emp_id']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>ชื่อ-นามสกุล :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['emp_name']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>ที่อยู่ :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['emp_address']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>เบอร์โทร :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['emp_tel']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>อีเมล์ :</strong><p></div>
        <div class="col-xs-6"><p><?php echo $row['emp_email']; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>วัน/เวลา ที่เพิ่ม :</strong><p></div>
        <div class="col-xs-6"><p><?php echo DatetimeThai($row['emp_registime']); ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>ประเภทผู้ใช้ :</strong></p></div>
        <div class="col-xs-6"><p><?php echo $emp_type[$row['emp_type']]; ?></p></div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2"><p><strong>สถานะ :</strong></p></div>
        <div class="col-xs-6"><p><?php echo $user_status[$row['emp_status']]; ?></p></div>
    </div>

</div>
