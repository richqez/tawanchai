<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';

if (isset($_POST['submit'])) {
    echo '<meta charset="utf-8">';
    $sql = 'SELECT * FROM employee WHERE emp_email = "' . $_POST['emp_email'] . '" AND emp_id != "' . $_SESSION['emp_id'] . '"';
    $recheackmail = mysql_query($sql);
    if (mysql_num_rows($recheackmail) > 0) {
        echo '<script>alert("อีเมล์นี้ มีผู้ใช้งานแล้ว !!!");window.history.back();</script>';
        exit();
    }

    $sql = 'UPDATE employee SET '
            . 'emp_name = "' . $_POST['emp_name'] . '", '
            . 'emp_address = "' . $_POST['emp_address'] . '", '
            . 'emp_tel = "' . $_POST['emp_tel'] . '", '
            . 'emp_email = "' . $_POST['emp_email'] . '" '
            . 'WHERE '
            . 'emp_id = "' . $_SESSION['emp_id'] . '" ';
    $result = mysql_query($sql);
    if ($result) {
        echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!")</script>';
        echo '<meta http-equiv="refresh" content="1; URL = profile.php"/>';
        exit();
    } else {
        echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ !!!");window.history.back();</script>';
        exit();
    }
}

if (isset($_POST['changepasssubmit'])) {
    echo '<meta charset="utf-8">';
    $sql = 'SELECT * FROM employee WHERE emp_id = "' . $_SESSION['emp_id'] . '" AND emp_password = "' . md5($_POST['emp_oldpassword']) . '"';
    $recheack = mysql_query($sql);
    if (mysql_num_rows($recheack) == 0) {
        echo '<script>alert("รหัสผ่านเดิมไม่ถูกต้อง !!!");window.history.back();</script>';
        exit();
    } else {
        $sql = 'UPDATE employee SET emp_password = "' . md5($_POST['emp_password']) . '" WHERE emp_id = "' . $_SESSION['emp_id'] . '"';
        $result = mysql_query($sql);
        if ($result) {
            echo '<script>alert("เปลี่ยนรหัสผ่านเรียบร้อยแล้ว กรุณาเข้าสู่ระบบอีกครั้ง !!!")</script>';
            echo '<meta http-equiv="refresh" content="1; URL = logout.php"/>';
            exit();
        } else {
            echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถเปลี่ยนรหัสผ่านได้ !!!");window.history.back();</script>';
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li class="active">ข้อมูลส่วนตัว</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">ข้อมูลส่วนตัว</h2>
                </div>
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> ข้อมูลส่วนตัว</div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3">
                                <?php
                                $sql = 'SELECT * FROM employee '
                                        . 'WHERE '
                                        . 'emp_id = "' . $_SESSION['emp_id'] . '"';
                                $result = mysql_query($sql);
                                $row = mysql_fetch_array($result);
                                ?>
                                <form name="employeeform" id="employeeform" action="" method="post">
                                    <div class="form-group">
                                        <input class="form-control" name="emp_username" type="text" id="emp_username" placeholder="ชื่อผู้ใช้ *" value="<?php echo $row['emp_username'] ?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="emp_name" type="text" id="emp_name" placeholder="ชื่อ-นามสกุล *" value="<?php echo $row['emp_name'] ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="emp_address" id="emp_address" placeholder="ที่อยู่ *"><?php echo $row['emp_address'] ?></textarea>    
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="emp_tel" type="text" id="emp_tel" placeholder="เบอร์โทรศัพท์ *" value="<?php echo $row['emp_tel'] ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="emp_email" type="email" id="emp_email" placeholder="อีเมล์ *" value="<?php echo $row['emp_email'] ?>"> 
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="emp_type" type="text" id="emp_type" placeholder="อีเมล์ *" value="<?php echo $emp_type[$row['emp_type']]; ?>" readonly> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> แก้ไขข้อมูลส่วนตัว</button>		
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-lock"></span> เปลี่ยนรหัสผ่าน</div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3">
                                <form name="changepassform" id="changepassform" action="" method="post">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="รหัสผ่านเก่า *" name="emp_oldpassword" id="emp_oldpassword" type="password" autofoemp="">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="รหัสผ่านใหม่ *" name="emp_password" id="emp_password" type="password" value="">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="ยืนยันรหัสผ่าน *" name="emp_password2" id="emp_password2" type="password" value="">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <button type="submit" class="btn btn-primary" name="changepasssubmit" id="changepasssubmit"><span class="glyphicon glyphicon-save"></span> เปลี่ยนรหัสผ่าน</button>		
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div><!-- /.row -->
        </div>	<!--/.main-->

        <!--<script src="js/jquery-1.11.1.min.js"></script>-->
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $.validator.addMethod("lettersonly", function (value, element, param) {
                    return value.match(new RegExp("." + param + "$"));
                });
                $('#changepassform').validate({
                    rules: {
                        emp_oldpassword: {
                            required: true
                        },
                        emp_password: {
                            required: true,
                            minlength: 8,
                            maxlength: 16
                        },
                        emp_password2: {
                            required: true,
                            equalTo: '#emp_password'
                        }
                    },
                    messages: {
                        emp_oldpassword: {
                            required: 'กรอกรหัสผ่านเดิม'
                        },
                        emp_password: {
                            required: 'กรอกรหัสผ่านใหม่',
                            minlength: 'รหัสผ่านไม่ต่ำกว่า 8 ตัวอักษร',
                            maxlength: 'ไม่เกิน 16 ตัวอักษร'
                        },
                        emp_password2: {
                            required: 'ยืนยันรหัสผ่านใหม่',
                            equalTo: 'พิมพ์รหัสผ่าน 2 ครั้ง ไม่ตรงกัน'
                        }
                    }
                });

                $('#employeeform').validate({
                    rules: {
                        emp_username: {
                            required: true,
                            minlength: 4,
                            maxlength: 12,
                            lettersonly: "[a-zA-Z]+"
                        },
                        emp_password: {
                            required: true,
                            minlength: 8,
                            maxlength: 16
                        },
                        emp_password2: {
                            required: true,
                            equalTo: '#emp_password'
                        },
                        emp_name: {
                            required: true
                        },
                        emp_address: {
                            required: true
                        },
                        emp_tel: {
                            required: true,
                            number: true,
                            minlength: 10
                        },
                        emp_email: {
                            required: true,
                            email: true
                        },
                        emp_type: {
                            required: true
                        }
                    },
                    messages: {
                        emp_username: {
                            required: 'กรอกชื่อผู้ใช้',
                            minlength: 'ไม่ต่ำกว่า 4 ตัวอักษร',
                            maxlength: 'ไม่เกิน 12 ตัวอักษร',
                            lettersonly: 'เป็นตัวอักษรภาษาอังกฤษเท่านั้น'
                        },
                        emp_password: {
                            required: 'กรอกรหัสผ่านใหม่',
                            minlength: 'รหัสผ่านไม่ต่ำกว่า 8 ตัวอักษร',
                            maxlength: 'ไม่เกิน 16 ตัวอักษร'
                        },
                        emp_password2: {
                            required: 'ยืนยันรหัสผ่านใหม่',
                            equalTo: 'พิมพ์รหัสผ่าน 2 ครั้ง ไม่ตรงกัน'
                        },
                        emp_name: {
                            required: 'กรอกชื่อ-นามสกุล'
                        },
                        emp_address: {
                            required: 'กรอกที่อยู่'
                        },
                        emp_tel: {
                            required: 'กรอกเบอร์โทรศัพท์',
                            number: 'เป็นตัวเลขเท่านั้น',
                            minlength: 'ต้องเป็น 10 หลัก'
                        },
                        emp_email: {
                            required: 'กรอกอีเมล์',
                            email: 'รูปแบบอีเมล์ไม่ถูกต้อง'
                        },
                        emp_type: {
                            required: 'เลือกประเภทพนักงาน'
                        }
                    }
                });
            });
        </script>
    </body>
</html>
