<?php
include 'lib/config.php';
include 'lib/conn.php';
include 'lib/function.php';
?>
<script type="text/javascript">
    $(function () {
        $('.usr').click(function () {
            $.post('driver_detail.php', {id: $(this).data('id')},
            function (data) {
                $('#cusdata').html(data);
            });
        });
    });
</script>

<?php
if ($_POST['page']) {
    $page = $_POST['page'];
    $cur_page = $page;
    $page -= 1;
    $per_page = 20;
    $previous_btn = TRUE;
    $next_btn = TRUE;
    $first_btn = TRUE;
    $last_btn = TRUE;
    $start = $page * $per_page;

    $opt = '';

    if ($_POST['search-text'] != '') {
        $opt .= ' AND driver.drv_name LIKE "%' . $_POST['search-text'] . '%" ';
        echo '<p align="center"><strong>ผลการค้นหา "' . $_POST['search-text'] . '"<br>';
        echo '<a href="">แสดงทั้งหมด</a></strong></p>';
    }
    ?>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="100" class="text-center">รหัสพนักงานขับรถ</th>
                    <th>ชื่อ-นามสกุล</th>
                    <th width="200" class="text-center">ประเภทรถบรรทุก</th>
                    <th width="100"class="text-center">รถบรรทุก</th>
                    <th width="220"class="text-center">วัน/เวลา ที่เพิ่ม</th>
                    <th width="100" class="text-center">รายละเอียด</th>
                    <th width="60" class="text-center">แก้ไข</th>
                    <th width="60" class="text-center">สถานะ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = 'SELECT * FROM driver, truck, trucktype, province '
                        . 'WHERE '
                        . 'driver.truck_id = truck.truck_id AND '
                        . 'truck.trucktype_id = trucktype.trucktype_id AND '
                        . 'truck.province_id = province.province_id ' . $opt
                        . 'ORDER BY driver.drv_id DESC LIMIT ' . $start . ',' . $per_page;
                $result = mysql_query($sql);
                if (mysql_num_rows($result) == 0) {
                    echo '<tr><td colspan="8" class="text-danger" align="center">ไม่พบข้อมูล</td></tr>';
                } else {
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $row['drv_id'] ?></td>
                            <td><?php echo $row['drv_name']; ?></td>
                            <td class="text-center"><?php echo $row['trucktype_name']; ?></td>
                            <td class="text-center">
                                <?php
                                if ($row['drv_status'] == '0') {
                                    echo '-';
                                } else {
                                    echo $row['truck_license'] . '<br>' . $row['province_name'];
                                }
                                ?>
                            </td>
                            <td class="text-center"><?php echo DatetimeThai($row['drv_registime']); ?></td>
                            <td class="text-center">
                                <a class="btn btn-info btn-sm usr" title="รายละเอียด" data-toggle="modal" data-target="#myModal" data-id="<?php echo $row['drv_id']; ?>">
                                    <span class="glyphicon glyphicon-th-large"></span>
                                </a> 
                            </td>
                            <td class="text-center">
                                <a class="btn btn-warning btn-sm" href="driver_edit.php?id=<?php echo $row['drv_id']; ?>" title="เปลี่ยนสถานะ"> <span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                            <td class="text-center">             
                                <a href="driver_status.php?id=<?php echo $row['drv_id']; ?>&status=<?php echo $row['drv_status']; ?>" title="เปลี่ยนสถานะ"  onclick="return confirm('ต้องการเปลี่ยนสถานะ ?');"><?php ShowStatus($row['drv_status']); ?></a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>

    <?php
    $query_pag_num = 'SELECT COUNT(*) as count FROM driver, truck, trucktype, province '
            . 'WHERE '
            . 'driver.truck_id = truck.truck_id AND '
            . 'truck.trucktype_id = trucktype.trucktype_id AND '
            . 'truck.province_id = province.province_id ' . $opt;
    $result_pag_num = mysql_query($query_pag_num);
    $row = mysql_fetch_array($result_pag_num);
    $count = $row['count'];
    $no_of_paginations = ceil($count / $per_page);

    include 'lib/pagination/pagination.php';
}