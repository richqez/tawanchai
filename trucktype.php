<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="lib/pagination/style.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li class="active">ประเภทรถบรรทุก</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ประเภทรถบรรทุก</h1>
                </div>
            </div><!--/.row-->

            <div class="row">
                <div class="col-md-3">
                    <button class="btn btn-default" type="button" onclick="window.location.href = 'trucktype_add.php'"> <span class="glyphicon glyphicon-plus"></span> เพิ่มประเภทรถบรรทุก</button>
                    <p>&nbsp;</p>
                </div>      
            </div> 
            <p>&nbsp;</p>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th width="100" class="text-center">ลำดับที่</th>
                            <th class="text-center">รหัสประเภทรถบรรทุก</th>
                            <th>ชื่อประเภทรถบรรทุก</th>
                            <th width="200" class="text-center">จำนวนรถบรรทุก</th>
                            <th width="80" class="text-center">แก้ไข</th>
                            <th width="80" class="text-center">ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = 'SELECT * FROM trucktype ';
                        $result = mysql_query($sql);
                        if (mysql_num_rows($result) == 0) {
                            echo '<tr><td colspan="5" class="text-center text-danger"><strong>==ไม่มีข้อมูล==</stron></td></tr>';
                        } else {
                            $i = 1;
                            while ($row = mysql_fetch_array($result)) {
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i ?></td>
                                    <td class="text-center"><?php echo $row['trucktype_id']; ?></td>
                                    <td><?php echo $row['trucktype_name']; ?></td>
                                    <td class="text-center">
                                        <?php
                                        $count = CountTruck($row['trucktype_id']);
                                        echo $count;
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" title="แก้ไข" class="btn btn-warning btn-sm" onclick="window.location.href = 'trucktype_edit.php?id=<?php echo $row['trucktype_id']; ?>'">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                    </td>
                                    <td class="text-center">
                                        <a href="trucktype_del.php?id=<?php echo $row['trucktype_id']; ?>" title="ลบ" class="btn btn-danger btn-sm<?php echo ($count > 0) ? ' disabled' : ''; ?>" onclick="return confirm('ต้องการลบ ?');">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="text-muted"><strong>หมายเหตุ</strong> : จะลบประเภทรถบรรทุกได้เมื่อไม่มีรถบรรทุกในประเภทนั้นๆ แล้ว</p>
                </div>
            </div>
        </div>
        
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-table.js"></script>
        <script type="text/javascript">
                                        !function ($) {
                                            $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                                                $(this).find('em:first').toggleClass("glyphicon-minus");
                                            });
                                            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
                                        }(window.jQuery);

                                        $(window).on('resize', function () {
                                            if ($(window).width() > 768)
                                                $('#sidebar-collapse').collapse('show');
                                        });
                                        $(window).on('resize', function () {
                                            if ($(window).width() <= 767)
                                                $('#sidebar-collapse').collapse('hide');
                                        });
        </script>
    </body>
</html>
