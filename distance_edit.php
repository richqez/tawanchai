<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/class.upload.php';

if (isset($_POST['submit'])) {
    echo '<meta charset="utf-8">';
    if (array_sum($_POST['distance_value']) == 0) {
        echo '<script>alert("เลือกจังหวัดปลายทาง !!!");window.history.back();</script>';
        exit();
    }
    $sql = 'DELETE FROM distance WHERE startpoint = "' . $_GET['id'] . '"';
    mysql_query($sql);

    foreach ($_POST['distance_value'] as $k => $v) {
        if ($v == 0) {
            continue;
        }

        $sql = 'INSERT INTO distance (startpoint, endpoint, distance_value) '
                . 'VALUES '
                . '("' . $_POST['startpoint'] . '", "' . $_POST['endpoint'][$k] . '", "' . $v . '")';
        mysql_query($sql);
    }

    echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!")</script>';
    echo '<meta http-equiv="refresh" content="1; URL = distance.php"/>';
    exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>


        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="distance.php">เส้นทางทางขนส่ง</a></li>
                    <li class="active">แก้ไขเส้นทางทางขนส่ง</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">เส้นทางทางขนส่ง</h2>
                </div>
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-plus"></span> แก้ไขเส้นทางทางขนส่ง</div>
                        <div class="panel-body">
                            <form name="distanceform" id="distanceform" action="" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="form-group">
                                            <label>ต้นทางจังหวัด</label>
                                            <?php
                                            $sql = 'SELECT * FROM province '
                                                    . 'WHERE '
                                                    . 'province_id = "' . $_GET['id'] . '"';
                                            $result = mysql_query($sql);

                                            $row = mysql_fetch_array($result);
                                            echo $row['province_name'];
                                            ?> 
                                            <input type="hidden" name="startpoint" id="startpoint" value="<?php echo $_GET['id']; ?>">
                                        </div>
                                    </div>
                                </div>

                                <p><strong>ปลายทาง</strong></p>
                                <hr>
                                <div class="row">
                                    <?php
                                    for ($i = 0; $i <= 60; $i+=30) {
                                        ?>
                                        <div class="col-md-4">
                                            <?php
                                            $sql = 'SELECT * FROM province '
                                                    . 'WHERE '
                                                    . 'province_id != "' . $_GET['id'] . '" '
                                                    . 'ORDER BY province_name ASC '
                                                    . 'LIMIT ' . $i . ',30';
                                            $result = mysql_query($sql);
                                            while ($row = mysql_fetch_array($result)) {
                                                $sql = 'SELECT * FROM distance '
                                                        . 'WHERE '
                                                        . 'startpoint = "' . $_GET['id'] . '" AND '
                                                        . 'endpoint = "' . $row['province_id'] . '"';
                                                $rs = mysql_query($sql);
                                                if (mysql_num_rows($rs) != 0) {
                                                    $r = mysql_fetch_array($rs);
                                                    $value = $r['distance_value'];
                                                } else {
                                                    $value = '';
                                                }
                                                ?>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <input type="hidden" name="endpoint[]" id="endpoint[]" value="<?php echo $row['province_id'] ?>"> <?php echo $row['province_name'] ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="number" min="0" class="form-control" name="distance_value[]" id="distance_value[]" placeholder="0" value="<?php echo $value; ?>">
                                                    </div>
                                                </div>
                                                <br>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    <?php } ?> 
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-1 col-sm-2 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> บันทึก</button>		
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
                                    </div>
                                </div>



                            </form>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div><!-- /.row -->
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('#distanceform').validate({
                    rules: {
                        startpoint: {
                            required: true
                        }
                    },
                    messages: {
                        startpoint: {
                            required: 'เลือกจังหวัดต้นทาง'
                        }
                    }
                });
            });
        </script>
    </body>
</html>
