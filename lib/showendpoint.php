<?php
include 'conn.php';
include 'config.php';

if ($_POST['startpoint'] != '') {
    ?>
    <p><strong>ปลายทาง</strong></p>
    <hr>
    <div class="row">
        <?php
        for ($i = 0; $i <= 60; $i+=30) {
            ?>
            <div class="col-md-4">
                <?php
                $sql = 'SELECT * FROM province '
                        . 'WHERE '
                        . 'province_id != "' . $_POST['startpoint'] . '" '
                        . 'ORDER BY province_name ASC '
                        . 'LIMIT ' . $i . ',30';
                $result = mysql_query($sql);
                while ($row = mysql_fetch_array($result)) {
                    ?>
                    <div class="row">
                        <div class="col-md-8">
                            <input type="hidden" name="endpoint[]" id="endpoint[]" value="<?php echo $row['province_id'] ?>"> <?php echo $row['province_name'] ?>
                        </div>
                        <div class="col-md-4">
                            <input type="number" min="0" class="form-control" name="distance_value[]" id="distance_value[]" placeholder="0">
                        </div>
                    </div>
                    <br>
                    <?php
                }
                ?>
            </div>
        <?php } ?> 
    </div>
    <hr>

    <div class="row">
        <div class="col-md-1 col-sm-2 col-md-offset-4">
            <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> บันทึก</button>		
        </div>
        <div class="col-md-2 col-sm-2">
            <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
        </div>
    </div>
    <?php
}