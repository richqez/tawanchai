<?php

function checklogin() {
    if (!isset($_SESSION['emp_id'])) {
        echo '<meta charset="utf-8">';
        echo '<meta http-equiv="refresh" content="0; URL = login.php"/>';
        exit();
    }
}

function ShowStatus($status) {
    if ($status == 0) {
        echo '<button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-lock"></span></button>';
    } else {
        echo '<button class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span></button>';
    }
}

function ShowTruckstatus($status) {
    global $truck_status;
    if ($status == 0) {
        echo '<button class="btn btn-danger btn-sm">' . $truck_status[$status] . '</button>';
    } else {
        echo '<button class="btn btn-success btn-sm">' . $truck_status[$status] . '</button>';
    }
}

function DisplayCardID($CardID) {
    echo substr($CardID, 0, 1) . " " . substr($CardID, 1, 4) . " " . substr($CardID, 5, 5) . " " . substr($CardID, 10, 2) . " " . substr($CardID, 12);
}

function ZeroFill($num, $zerofill = 4) {
    return str_pad($num, $zerofill, '0', STR_PAD_LEFT); //เติมเลข 0 ให้เต็มทางซ้ายตามที่ต้องการ
}

function DateFormatDB($date) {
    $find = explode("/", $date);
    $date = $find[2] . '-' . $find[1] . '-' . $find[0];
    return $date;
}

function DateFormatTextbox($date) {
    $find = explode("-", $date);
    $date = $find[2] . '/' . $find[1] . '/' . $find[0];
    return $date;
}

function ThaidateNoTime($date) {
    if ($date == '') {
        return '';
    } else {
        $find = explode("-", $date);
        $month = array("ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $find[1] -= 1;
        $day = explode(" ", $find[2]);
        $thaidate = (($day[0] * 10) / 10) . "  " . $month[$find[1]] . " " . ($find[0] + 543);
        return $thaidate;
    }
}

function ThaidateNoTime2($date) {
    if ($date == '') {
        return '';
    } else {
        $find = explode("/", $date);
        $month = array("ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $find[1] -= 1;
        $day = explode(" ", $find[0]);
        $thaidate = (($day[0] * 10) / 10) . "  " . $month[$find[1]] . " " . ($find[2] + 543);
        return $thaidate;
    }
}

function TimeNodate($date) {
    $find = explode(" ", $date);
    return $find[1];
}

function DatetimeThai($datetime) {
    $date = explode(" ", $datetime);
    $find = explode("-", $date[0]);
    $month = array("ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
    $find[1] -= 1;
    $day = explode(" ", $find[2]);
    $thaidate = (($day[0] * 10) / 10) . "  " . $month[$find[1]] . " " . ($find[0] + 543);
    return $thaidate . ' เวลา ' . $date[1] . ' น.';
}

function BankName() {
    $bank = array(
        'ธนาคารกรุงไทย',
        'ธนาคารกรุงเทพ',
        'ธนาคารกสิกรไทย',
        'ธนาคารไทยพาณิชย์',
        'ธนาคารทหารไทย'
    );

    echo '<option value="">เลือกธนาคาร</option>';
    foreach ($bank as $k => $v) {
        echo '<option value="' . $v . '">' . $v . '</option>';
    }
}

function Employeetype($usertype = '') {
    global $emp_type;
    echo '<option value="">ประเภทพนักงาน</option>';
    foreach ($emp_type as $k => $v) {
        if ($k == $usertype) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        echo '<option value="' . $k . '"' . $selected . '>' . $v . '</option>';
    }
}

function NextEmp_id() {
    $sql = 'SELECT MAX(emp_id) AS id FROM employee';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    if ($row['id'] == '') {
        return 'E0001';
    } else {
        $cid = substr($row['id'], 1);
        return 'E' . ZeroFill($cid + 1);
    }
}

function NextCus_id() {
    $sql = 'SELECT MAX(cus_id) AS id FROM customer';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    if ($row['id'] == '') {
        return 'C0001';
    } else {
        $cid = substr($row['id'], 1);
        return 'C' . ZeroFill($cid + 1);
    }
}

function NextDrv_id() {
    $sql = 'SELECT MAX(drv_id) AS id FROM driver';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    if ($row['id'] == '') {
        return 'D0001';
    } else {
        $cid = substr($row['id'], 1);
        return 'D' . ZeroFill($cid + 1);
    }
}

function NextTruck_id($type) {
    $sql = 'SELECT MAX(truck_id) AS id FROM truck WHERE trucktype_id = "' . $type . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    if ($row['id'] == '') {
        return $type . '-0001';
    } else {
        $cid = explode("-", $row['id']);
        return $type . '-' . ZeroFill($cid[1] + 1);
    }
}

function CountTruck($trucktype_id) {
    $sql = 'SELECT * FROM truck WHERE trucktype_id = "' . $trucktype_id . '"';
    $result = mysql_query($sql);
    return mysql_num_rows($result);
}

function CountFuel($fuel_id) {
    $sql = 'SELECT * FROM truck WHERE fuel_id = "' . $fuel_id . '"';
    $result = mysql_query($sql);
    return mysql_num_rows($result);
}

function Trucktype_id($id = '') {
    echo '<option value="">ทุกประเภทรถบรรทุก</option>';
    $sql = 'SELECT * FROM trucktype ';
    $result = mysql_query($sql);
    while ($row = mysql_fetch_array($result)) {
        if ($id == $row['trucktype_id']) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        echo '<option value="' . $row['trucktype_id'] . '"' . $selected . '>' . $row['trucktype_name'] . '</option>';
    }
}

function Fuel_id($id = '') {
    echo '<option value="">ทุกเชื้อเพลิง</option>';
    $sql = 'SELECT * FROM fuel';
    $result = mysql_query($sql);
    while ($row = mysql_fetch_array($result)) {
        if ($id == $row['fuel_id']) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        echo '<option value="' . $row['fuel_id'] . '"' . $selected . '>' . $row['fuel_name'] . '</option>';
    }
}

function Driver_id($id = '') {
    echo '<option value="">ทุกพนักงานขับรถ</option>';
    $sql = 'SELECT * FROM driver';
    $result = mysql_query($sql);
    while ($row = mysql_fetch_array($result)) {
        if ($id == $row['drv_id']) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        echo '<option value="' . $row['drv_id'] . '"' . $selected . '>' . $row['drv_name'] . '</option>';
    }
}

function Province($id = '') {
    echo '<option value="">เลือกจังหวัด</option>';
    $sql = 'SELECT * FROM province';
    $result = mysql_query($sql);
    while ($row = mysql_fetch_array($result)) {
        if ($id == $row['province_id']) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        echo '<option value="' . $row['province_id'] . '"' . $selected . '>' . $row['province_name'] . '</option>';
    }
}

function Getdriver($truck_id) {
    $sql = 'SELECT * FROM driver WHERE truck_id = "' . $truck_id . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    if (mysql_num_rows($result) == 0) {
        return;
    } else {
        return $row['drv_name'];
    }
}

function Unit($value = '') {
    $arr = array('กล่อง', 'ลัง', 'พาเลท','ชิ้น','มัด','ม้วน','หลัง','แผ่น','ใบ');
    echo '<option value="">เลือกหน่วย</option>';
    $sql = 'SELECT * FROM province';
    foreach ($arr as $v) {
        if ($v == $value) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        echo '<option value="' . $v . '" ' . $selected . '>' . $v . '</option>';
    }
}

function Startpoint($value = '') {
    $sql = 'SELECT * FROM distance, province WHERE distance.startpoint = province.province_id GROUP BY distance.startpoint ORDER BY `province`.`province_name` ASC';
    $result = mysql_query($sql);
    echo '<option value="">เลือกต้นทาง</option>';
    while ($row = mysql_fetch_array($result)) {
        if ($row['startpoint'] == $value) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        echo '<option value="' . $row['startpoint'] . '" ' . $selected . '>' . $row['province_name'] . '</option>';
    }
}

function NextQuo_id() {
    $sql = 'SELECT MAX(quo_id) AS quo_id FROM quotation ORDER BY quo_date DESC';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    if (mysql_num_rows($result) == 0) {
        return 'Q0001/' . (date('Y') + 543);
    } else {
        $cid = explode("/", $row['quo_id']);
        $c_id = substr($cid[0], 1);
        return 'Q' . ZeroFill($c_id + 1) . '/' . (date('Y') + 543);
    }
}

function NextInvoice_id() {
    $sql = 'SELECT MAX(invoice_id) AS invoice_id FROM invoice ORDER BY invoice_date DESC';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    if (mysql_num_rows($result) == 0) {
        return 'I0001/' . (date('Y') + 543);
    } else {
        $cid = explode("/", $row['invoice_id']);
        $c_id = substr($cid[0], 1);
        return 'I' . ZeroFill($c_id + 1) . '/' . (date('Y') + 543);
    }
}

function NextPayment_id() {
    $sql = 'SELECT MAX(payment_id) AS payment_id FROM payment ORDER BY payment_date DESC';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    if (mysql_num_rows($result) == 0) {
        return 'P0001/' . (date('Y') + 543);
    } else {
        $cid = explode("/", $row['payment_id']);
        $c_id = substr($cid[0], 1);
        return 'P' . ZeroFill($c_id + 1) . '/' . (date('Y') + 543);
    }
}

function Province_name($id) {
    $sql = 'SELECT * FROM province WHERE province_id = "' . $id . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    return $row['province_name'];
}

function GetTrucktype($id) {
    $sql = 'SELECT * FROM trucktype WHERE trucktype_id = "' . $id . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    return $row['trucktype_name'];
}

function GetTruck($id) {
    $sql = 'SELECT * FROM truck WHERE truck_id = "' . $id . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    return $row['truck_license'] . ' ' . Province_name($row['province_id']);
}

function GetDriver_idByname($d_name) {
    $sql = 'SELECT * FROM driver WHERE drv_name = "' . $d_name . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    return $row['drv_id'];
}

function Getdriver2($drv_id) {
    $sql = 'SELECT * FROM driver WHERE drv_id = "' . $drv_id . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    return $row['drv_name'];
}

//ค่าขนส่งต่อเที่ยว
//ระยะทาง, ราคาน้ำมัน, รหัสรถบรรทุก
function Quo_price($dis, $fuel_price, $truck_id) {
    /*
     * จำนวนน้ำมัน/เที่ยว = ระยะทาง / สิ้นเปลืองน้ำมัน กม:ลิคร
     * 
     * ค่าน้ำมัน/เที่ยว = จำนวนน้ำมัน/เที่ยว * ราคาน้ำมันต่อลิตร
     * 
     * ค่าขนส่ง/เที่ยว = ค่าเช่า + ค่าน้ำมัน/เที่ยว + ค่าแรงคนขับต่อเที่ยว (300)
     * เปอร์เซ็นต์คนขับ = ค่าขนส่ง/เที่ยว * 10%
     * 
     * ค่าขนส่ง/เที่ยว = ค่าขนส่ง/เที่ยว + เปอร์เซ็นต์คนขับ
     * 
     */

    $drv_price = 300;
    $sql = 'SELECT * FROM truck WHERE truck_id = "' . $truck_id . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    $truck_rent = $row['truck_rent']; //ค่าเช่า
    $truck_fueluse = $row['truck_fueluse']; //ใช้น้ำมัน กม./ลิตร

    $fuel_value = $dis / $truck_fueluse; //จำนวนน้ำมัน/เที่ยว
    $fuel = $fuel_value * $fuel_price; //ค่าน้ำมัน/เที่ยว

    $quo_price = $truck_rent + $fuel + $drv_price;
    $per = $quo_price * 0.10;
    $quo_price += $per;

    //ปรับราคา
    $rs = $quo_price / 500;
    if (!is_int($rs)) {
        $quo_price = ceil($rs) * 500;
    }

    $quo = array();
    $quo['quo_fuel_value'] = $fuel_value;
    $quo['quo_fuel_price'] = $fuel_price;
    $quo['quo_fuel_total'] = $fuel;
    $quo['quo_rent'] = $truck_rent;
    $quo['quo_drv_price'] = $drv_price;
    $quo['quo_drv_per'] = $per;
    $quo['quo_price'] = $quo_price;

    return $quo;
}

function TruckInfo($trucek_id) {
    $rs = array();
    $sql = 'SELECT * FROM truck, trucktype, fuel, province '
            . 'WHERE '
            . 'truck.fuel_id = fuel.fuel_id AND '
            . 'trucktype.trucktype_id = truck.trucktype_id AND '
            . 'province.province_id = truck.province_id AND '
            . 'truck.truck_id = "' . $trucek_id . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    $rs['fuel_name'] = $row['fuel_name'];
    $rs['fuel_name'] = $row['fuel_name'];

    return $rs;
}

function Fuel_Name($id) {
    $sql = 'SELECT * FROM fuel WHERE fuel_id = "' . $id . '"';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    return $row['fuel_name'];
}
