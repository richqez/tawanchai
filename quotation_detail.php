<?php
session_start();
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/function.php';
?>
<div class="row">
    <div class="col-md-12 text-center">
        <?php echo COMPANY_NAME; ?>

        <h2>ใบเสนอราคา</h2>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?php
        $sql = 'SELECT * FROM customer, quotation WHERE customer.cus_id = quotation.cus_id AND quotation.quo_id = "' . $_POST['id'] . '"';
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result);
        ?>
        <p><strong>รหัสลูกค้า : </strong><?php echo $row['cus_id']; ?></p>
        <p><strong>ชื่อบริษัท : </strong><?php echo $row['cus_name']; ?></p>
        <p><strong>ที่อยู่ : </strong><?php echo $row['cus_address']; ?></p>
        <p><strong>เบอร์โทร : </strong><?php echo $row['cus_tel']; ?></p>
        <p><strong>E-mail : </strong><?php echo $row['cus_email']; ?> <strong>Line ID : </strong><?php echo $row['cus_line']; ?></p>
        <p><strong>ผู้ประสานงาน : </strong><?php echo $row['cus_contact']; ?></p>
    </div>
    <div class="col-md-6 text-right">
        <p><strong>เลขที่ : </strong><?php echo $row['quo_id'] ?></p>
        <p><strong>วันที่ : </strong><?php echo ThaidateNoTime($row['quo_date']); ?></p>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center">รายละเอียด</th>
                <th width="100" class="text-center">จำนวนเที่ยว</th>
                <th width="200" class="text-center">ราคาต่อเที่ยว</th>
                <th width="200" class="text-center">ราคารวม</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?php
                    echo '<p><strong>ข้อมูลปลายทาง</strong></p>';
                    echo 'ชื่อ-นามสกุล : ' . $row['quo_name'] . ' <br>';
                    echo 'ที่อยู่ : ' . $row['quo_address'] . ' <br>';
                    echo 'เบอร์โทร : ' . $row['quo_tel'] . ' <br>';
                    echo 'E-mail : ' . $row['quo_mail'] . ' Line ID : ' . $row['quo_line'] . '<br><br>';

                    echo '<p><strong>รายละเอียดสินค้า</strong></p>';
                    echo 'ขนส่ง ' . $row['quo_product'] . ' ';
                    echo 'จำนวน ' . $row['quo_count'] . ' ' . $row['quo_unit'] . ' ';
                    echo '<br>';
                    echo 'ต้นทาง ' . Province_name($row['startpoint']) . ' ถึงปลายทาง ' . Province_name($row['endpoint']) . ' ';
                    echo '<br>';
                    echo 'รวมระยะทาง ' . $row['quo_distance'] . ' กิโลเมตร';
                    echo '<br>';
                    echo 'ตั้งแต่วันที่ ' . ThaidateNoTime($row['quo_startdate']) . ' ถึงวันที่ ' . ThaidateNoTime($row['quo_enddate']) . ' ';
                    echo '<br>';
                    echo '<br>';
                    echo 'ใช้ ' . GetTrucktype($row['trucktype_id']) . ' ทะเบียนรถ ' . GetTruck($row['truck_id']);
                    echo '<br>';
                    echo 'พนักงานขับรถ ' . Getdriver2($row['drv_id']);
                    ?>
                </td>
                <td class="text-center"><?php echo $row['quo_number']; ?></td>
                <td class="text-center">
                    <?php
                    echo number_format($row['quo_price'], 2);
                    ?>
                </td>
                <td class="text-right">
                    <?php
                    echo number_format($row['quo_total'], 2);
                    ?>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="text-right">
                    <strong>รวมทั้งสิ้น</strong>
                </td>
                <td class="text-right"><strong><?php echo number_format($row['quo_total'], 2); ?></strong></td>
            </tr>
        </tfoot>
    </table>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <h4>สถานะ : <?php echo $quo_status[$row['quo_status']]; ?></h4>
    </div>
</div>

<?php
if ($_SESSION['emp_type'] == '2') {
    if ($row['quo_status'] == '0') {
        ?>

        <div class="row">
            <hr>
            <div class="col-md-12 text-center">
                <a class="btn btn-primary" href="quotation_status.php?id=<?php echo $row['quo_id'] ?>&status=1" title="ยกเลิก" onclick="return confirm('อนุมัติใบเสนอราคานี้ใช่หรือไม่ ?');"><span class="glyphicon glyphicon-check"></span> ยืนยัน</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href="quotation_status.php?id=<?php echo $row['quo_id'] ?>&status=2" title="ยกเลิก" onclick="return confirm('ยกเลิกใบเสนอราคานี้ใช่หรือไม่ ?');"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</a>
            </div>
        </div>
        <?php
    }
}