<?php
session_start();
include 'lib/config.php';
include 'lib/conn.php';
include 'lib/function.php';
?>
<script type="text/javascript">
    $(function () {
        $('.usr').click(function () {
            $.post('employee_detail.php', {id: $(this).data('id')},
            function (data) {
                $('#cusdata').html(data);
            });
        });
    });
</script>

<?php
if ($_POST['page']) {
    $page = $_POST['page'];
    $cur_page = $page;
    $page -= 1;
    $per_page = 20;
    $previous_btn = TRUE;
    $next_btn = TRUE;
    $first_btn = TRUE;
    $last_btn = TRUE;
    $start = $page * $per_page;

    $opt = '';

    if ($_POST['search-text'] != '') {
        $opt .= ' AND (emp_name LIKE "%' . $_POST['search-text'] . '%" OR ';
        $opt .= 'emp_username LIKE "%' . $_POST['search-text'] . '%")';
        echo '<p align="center"><strong>ผลการค้นหา "' . $_POST['search-text'] . '"<br>';
        echo '<a href="">แสดงทั้งหมด</a></strong></p>';
    }
    ?>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="100" class="text-center">รหัสพนักงาน</th>
                    <th width="100" class="text-center">ชื่อผู้ใช้</th>
                    <th>ชื่อ-นามสกุล</th>
                    <th width="100" class="text-center">ประเภท</th>
                    <th width="220"class="text-center">วัน/เวลา ที่เพิ่ม</th>
                    <th width="100" class="text-center">รายละเอียด</th>
                    <?php
                    if ($_SESSION['emp_type'] == '3') {
                        ?>
                        <th width="80" class="text-center">แก้ไข</th>
                        <th width="80" class="text-center">สถานะ</th>
                        <th width="110" class="text-center">รีเซ็ตรหัสผ่าน</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = 'SELECT * FROM employee '
                        . 'WHERE '
                        . '1 = 1 ' . $opt
                        . 'ORDER BY emp_id DESC '
                        . 'LIMIT ' . $start . ',' . $per_page;
                $result = mysql_query($sql);
                if (mysql_num_rows($result) == 0) {
                    echo '<tr><td colspan="8" class="text-danger" align="center">ไม่พบข้อมูล</td></tr>';
                } else {
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $row['emp_id'] ?></td>
                            <td class="text-center"><?php echo $row['emp_username'] ?></td>
                            <td><?php echo $row['emp_name']; ?></td>
                            <td class="text-center"><?php echo $emp_type[$row['emp_type']]; ?></td>
                            <td class="text-center"><?php echo DatetimeThai($row['emp_registime']); ?></td>
                            <td class="text-center">
                                <a class="btn btn-info btn-sm usr" title="รายละเอียด" data-toggle="modal" data-target="#myModal" data-id="<?php echo $row['emp_id']; ?>">
                                    <span class="glyphicon glyphicon-th-large"></span>
                                </a> 
                            </td>
                            <?php
                            if ($_SESSION['emp_type'] == '3') {
                                ?>
                                <td class="text-center">
                                    <a class="btn btn-warning btn-sm" href="employee_edit.php?id=<?php echo $row['emp_id']; ?>" title="เปลี่ยนสถานะ"> <span class="glyphicon glyphicon-pencil"></span></a>
                                </td>
                                <td class="text-center">
                                    <?php
                                    if ($row['emp_type'] == 1 || $row['emp_type'] == 2) {
                                        ?>
                                        <a href="employee_status.php?id=<?php echo $row['emp_id']; ?>&status=<?php echo $row['emp_status']; ?>" title="เปลี่ยนสถานะ"  onclick="return confirm('ต้องการเปลี่ยนสถานะ ?');"><?php ShowStatus($row['emp_status']); ?></a>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php
                                    if ($row['emp_type'] == 1 || $row['emp_type'] == 2) {
                                        ?>
                                        <a class="btn btn-danger btn-sm" href="employee_reset.php?id=<?php echo $row['emp_id']; ?>" title="เปลี่ยนสถานะ" onclick="return confirm('ต้องการรีเซ็ตรหัสผ่าน ?');"> <span class="glyphicon glyphicon-refresh"></span></a>
                                            <?php
                                        }
                                        ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>

    <?php
    $query_pag_num = 'SELECT COUNT(*) AS count FROM employee '
            . ' WHERE '
            . ' 1 = 1 ' . $opt;
    $result_pag_num = mysql_query($query_pag_num);
    $row = mysql_fetch_array($result_pag_num);
    $count = $row['count'];
    $no_of_paginations = ceil($count / $per_page);

    include 'lib/pagination/pagination.php';
}