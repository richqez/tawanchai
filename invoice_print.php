<?php

include 'lib/conn.php';
include 'lib/config.php';
include 'lib/function.php';
$sql = 'SELECT * FROM customer, invoice '
        . 'WHERE '
        . 'customer.cus_id = invoice.cus_id AND '
        . 'invoice.invoice_id = "' . $_GET['id'] . '" ';
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
$iv_status = $row['invoice_status'];
$html = '<body style="font-size: 10pt;">';
$htmlh = '<p align="center">' . COMPANY_NAME . '<h2 align="center">ใบแจ้งหนี้</h2>';
$htmlh .= '<h2 align="center">ค่าขนส่งประจำเดือน ' . $month_short[$row['invoice_month']] . ' ' . ($row['invoice_year'] + 543) . '</h2>';
$htmlh .= '</p>';

$htmlh .= '<table width="100%" align="center" style="line-height:2em;">';
$htmlh .= '<tr>';
$htmlh .= '<td valign="top">';
$htmlh .= '<p><strong>รหัสลูกค้า : </strong>' . $row['cus_id'] . '</p>';
$htmlh .= '<p><strong>ชื่อบริษัท : </strong>' . $row['cus_name'] . '</p>';
$htmlh .= '<p><strong>ที่อยู่ : </strong>' . $row['cus_address'] . '</p>';
$htmlh .= '<p><strong>เบอร์โทร : </strong>' . $row['cus_tel'] . '</p>';
$htmlh .= '<p><strong>E-mail : </strong>' . $row['cus_email'] . ' <strong>Line ID : </strong>' . $row['cus_line'] . '</p>';
$htmlh .= '<p><strong>ผู้ประสานงาน : </strong>' . $row['cus_contact'] . '</p>';
$htmlh .= '</td>';
$htmlh .= '<td align="right" valign="top">';
$htmlh .= '<p><strong>เลขที่ : </strong>' . $row['invoice_id'] . '</p>';
$htmlh .= '<p><strong>วันที่ : </strong>' . ThaidateNoTime($row['invoice_date']) . '</p>';
$htmlh .= '</td>';
$htmlh .= '</tr>';
$htmlh .= '</table>';
// $html .= '<br>';
$html .= '<table width="100%" border="1" align="center" style="line-height:2em; border-collapse: collapse;">';
$html .= '<tr>';
$html .= '<th width="50">ลำดับที่</th>';
$html .= '<th width="100">วันที่</th>';
$html .= '<th>รายละเอียด</th>';
$html .= '<th width="80">จำนวนเที่ยว</th>';
$html .= '<th width="80">ราคาต่อเที่ยว</th>';
$html .= '<th width="100">ราคารวม</th>';
$html .= '</tr>';
$sql = 'SELECT * FROM quotation, invoice, invoicedetail '
        . 'WHERE '
        . 'invoice.invoice_id = invoicedetail.invoice_id AND '
        . 'invoicedetail.quo_id = quotation.quo_id AND '
        . 'invoice.invoice_id = "' . $_GET['id'] . '" '
        . 'ORDER BY quotation.quo_date ASC ';
$result = mysql_query($sql);
$i = 1;
$quo_total = 0;
while ($row = mysql_fetch_array($result)) {
    if($i != "1"){
         
      if (mb_strlen($row['quo_product'],"UTF-8") >= 75) {
            $html .= '<tr class="blank_row">';
            $html .= '<td style="border-right: white solid 1px;border-left: white solid 1px;border-bottom: white solid 1px;" height="20"  colspan="3"></td>';
            $html .= '</tr>';
            $html .= '<tr>';
              $html .= '<th width="50">ลำดับที่</th>';
              $html .= '<th width="100">วันที่</th>';
              $html .= '<th>รายละเอียด</th>';
              $html .= '<th width="80">จำนวนเที่ยว</th>';
              $html .= '<th width="80">ราคาต่อเที่ยว</th>';
              $html .= '<th width="100">ราคารวม</th>';
              $html .= '</tr>';
        }else if(mb_strlen($row['quo_product'],"UTF-8") > 41 && strlen($row['quo_product']) <= 54 ){
            $html .= '<tr class="blank_row">';
            $html .= '<td style="border-right: white solid 1px;border-left: white solid 1px;border-bottom: white solid 1px;" height="95"  colspan="3"></td>';
            $html .= '</tr>';

            $html .= '<tr>';
              $html .= '<th width="50">ลำดับที่</th>';
              $html .= '<th width="100">วันที่</th>';
              $html .= '<th>รายละเอียด</th>';
              $html .= '<th width="80">จำนวนเที่ยว</th>';
              $html .= '<th width="80">ราคาต่อเที่ยว</th>';
              $html .= '<th width="100">ราคารวม</th>';
              $html .= '</tr>';
        }else if(mb_strlen($row['quo_product'],"UTF-8") <= 41){
            $html .= '<tr class="blank_row">';
            $html .= '<td style="border-right: white solid 1px;border-left: white solid 1px;border-bottom: white solid 1px;" height="120"  colspan="3"></td>';
            $html .= '</tr>';

            $html .= '<tr>';
              $html .= '<th width="50">ลำดับที่</th>';
              $html .= '<th width="100">วันที่</th>';
              $html .= '<th>รายละเอียด</th>';
              $html .= '<th width="80">จำนวนเที่ยว</th>';
              $html .= '<th width="80">ราคาต่อเที่ยว</th>';
              $html .= '<th width="100">ราคารวม</th>';
              $html .= '</tr>';
        }

      

    }
    $quo_total += $row['quo_total'];
    $html .= '<tr>';
    $html .= '<td align="center" valign="top">' . $i . '</td>';
    $html .= '<td align="center" valign="top">' . ThaidateNoTime($row['quo_date']) .'</td>';
    $html .= '<td>';
    $html .= '<p><strong>ข้อมูลปลายทาง</strong></p>';
    $html .= 'ชื่อ-นามสกุล : ' . $row['quo_name'] . ' <br>';
    $html .= 'ที่อยู่ : ' . $row['quo_address'] . ' <br>';
    $html .= 'เบอร์โทร : ' . $row['quo_tel'] . ' <br>';
    $html .= 'E-mail : ' . $row['quo_mail'] . '<br> Line ID : ' . $row['quo_line'] . '<br><br>';
    $html .= '<p><strong>รายละเอียดสินค้า</strong></p>';
    $html .= 'ขนส่ง ' . $row['quo_product'] . ' ';
    $html .= 'จำนวน ' . $row['quo_count'] . ' ' . $row['quo_unit'] . ' ';
    $html .= '<br>';
    $html .= 'ต้นทาง ' . Province_name($row['startpoint']) . ' ถึงปลายทาง ' . Province_name($row['endpoint']) . ' ';
    $html .= '<br>';
    $html .= 'รวมระยะทาง ' . $row['quo_distance'] . ' กิโลเมตร';
    $html .= '<br>';
    $html .= 'ตั้งแต่วันที่ ' . ThaidateNoTime($row['quo_startdate']) . ' ถึงวันที่ ' . ThaidateNoTime($row['quo_enddate']) . ' ';
    $html .= '<br>';
    $html .= '<br>';
    $html .= 'ใช้ ' . GetTrucktype($row['trucktype_id']) . ' ทะเบียนรถ ' . GetTruck($row['truck_id']);
    $html .= '<br>';
    $html .= 'พนักงานขับรถ ' . Getdriver2($row['drv_id']);
    $html .= '</td>';
    $html .= '<td valign="top" align="center">';
    $html .= $row['quo_number'];
    $html .= '</td>';
    $html .= '<td valign="top" align="right">';
    $html .= number_format($row['quo_price'], 2);
    $html .= '</td>';
    $html .= '<td valign="top" align="right">';
    $html .= number_format($row['quo_total'], 2);
    $html .= '</td>';
    $html .= '</tr>';
    $i++;

}
$html .= '<tr>';
$html .= '<td colspan="5" align="right">';
$html .= '<strong>รวมทั้งสิ้น</strong>';
$html .= '</td>';
$html .= '<td align="right"><strong>' . number_format($quo_total, 2) . '</strong></td>';
$html .= '</tr>';
$html .= '</table>';

$html .= '<table width="100%" align="center" style="line-height:2em;">';
$html .= '<tr>';
$html .= '<td width="60%">';
$html .= '</td>';
$html .= '<td align="center">';
$html .= '<p>ลงชื่อ</p>';
$html .= '<p>...........................................</p>';
$html .= '<p>(...........................................)</p>';
$html .= '</td>';
$html .= '</tr>';
$html .= '</table>';
$html .= '</body>';

include("lib/mpdf/mpdf.php");

$mpdf = new mPDF('UTF-8');
$mpdf = new mPDF('th', A4, '', 'angsanaupc');
$mpdf->SetHTMLHeader($htmlh);
$mpdf->setFooter("หน้า {PAGENO} of {nb}");
$mpdf->PDFAauto = true;
$mpdf->SetTopMargin(105);
$mpdf->SetAutoFont();
// $mpdf->SetAutoPageBreak($auto);
$mpdf->WriteHTML($html);
$mpdf->Output();

exit;
