<?php
session_start();
include 'lib/function.php';
checklogin();
include 'lib/conn.php';
include 'lib/config.php';
include 'lib/class.upload.php';

if (isset($_POST['submit'])) {
    echo '<meta charset="utf-8">';
    $sql = 'INSERT INTO trucktype (trucktype_id, trucktype_name) VALUES ("' . trim($_POST['trucktype_id']) . '",'
            . '"' . trim($_POST['trucktype_name']) . '")';
    $result = mysql_query($sql);
    if ($result) {
        echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว !!!")</script>';
        echo '<meta http-equiv="refresh" content="1; URL = trucktype.php"/>';
        exit();
    } else {
        echo '<script>alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ !!!");window.history.back();</script>';
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo SYS_NAME; ?></title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="mycss/Mystyle.css" rel="stylesheet" type="text/css"/>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <?php
        include 'lib/head.php';
        include 'lib/menuleft.php';
        ?>


        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span></a></li>
                    <li><a href="trucktype.php">ประเภทรถบรรทุก</a></li>
                    <li class="active">เพิ่มประเภทรถบรรทุก</li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">ประเภทรถบรรทุก</h2>
                </div>
            </div><!--/.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span class="glyphicon glyphicon-plus"></span> เพิ่มประเภทรถบรรทุก</div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3">
                                <form name="trucktypeform" id="trucktypeform" action="" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>รหัสประเภทรถบรรทุก</label>
                                        <input class="form-control" name="trucktype_id" type="text" id="trucktype_id" placeholder="รหัสประเภทรถบรรทุก *" maxlength="3"> 
                                    </div>
                                    <div class="form-group">
                                        <label>ชื่อประเภทรถบรรทุก</label>
                                        <input class="form-control" name="trucktype_name" type="text" id="trucktype_name" placeholder="ชื่อประเภทรถบรรทุก *"> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn btn-primary" name="submit" id="submit"><span class="glyphicon glyphicon-save"></span> บันทึก</button>		
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="reset" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> ยกเลิก</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col-->
            </div><!-- /.row -->
        </div>	<!--/.main-->

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/additional-methods.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('#trucktypeform').validate({
                    rules: {
                        trucktype_id: {
                            required: true,
                            minlength: 3
                        },
                        trucktype_name: {
                            required: true
                        }
                    },
                    messages: {
                        trucktype_id: {
                            required: 'กรอกรหัสประเภทรถบรรทุก',
                            minlength: 'ต้องเป็นตัวอักษร 3 ตัวอักษร'
                        }, trucktype_name: {
                            required: 'กรอกชื่อประเภทรถบรรทุก'
                        }
                    }
                });
            });
        </script>
    </body>
</html>
